﻿using UnityEngine;
using UnityEngine.UI;

public class GameTimeManager : MonoBehaviour {

	public bool throughSlider = true;

	public Slider timeScaleSlider;
	public AnimationCurve timeScaleCurve;

	private float _timer = 0.0f;
    	void Update() {
		if (throughSlider){
			Time.timeScale = timeScaleSlider.value;
		} else {
			_timer += Time.deltaTime;
			Time.timeScale = timeScaleCurve.Evaluate(_timer);	
		}
    	}
}
