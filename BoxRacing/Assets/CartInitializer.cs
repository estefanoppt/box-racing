using UnityEngine;

/*
 * A simple MonoBehavior for initializing the carts of the
 * Scene to different acceleration values.
 */
public class CartInitializer : MonoBehaviour {

	public AnimationCurve isCleverAccelerationValues;
	public AnimationCurve isntCleverAccelerationValues;

	public void Awake(){
		Cart[] toInitialize = GameObject.FindObjectsOfType<Cart>();
		for (int i = 0; i < toInitialize.Length; i++){
			if (toInitialize[i].tag == "Player") continue;
			
			AmbientStateSampler sampler = toInitialize[i].GetComponent<AmbientStateSampler>();

			if (sampler.isClever){
				toInitialize[i].acceleration = isCleverAccelerationValues.Evaluate((float) i / (float) toInitialize.Length);
			} else {
				toInitialize[i].acceleration = isntCleverAccelerationValues.Evaluate((float) i / (float) toInitialize.Length);
			}
		}
	}
}
