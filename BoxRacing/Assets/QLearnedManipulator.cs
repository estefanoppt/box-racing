using System;
using UnityEngine;

/*
 * Uses the Q-Learning structure to provide suggestions as to 
 * what actions the Cart has to take. 
 */
[RequireComponent(typeof(AmbientStateSampler), typeof(Cart))]
public class QLearnedManipulator : MonoBehaviour {

	/*
	 * ========================================================================================
	 * Unity Interface Variables
	 * ========================================================================================
	 */

	// Reference to the QLearning database. 
	public MovementQLearning movementQLearning;
	public TrackQLearning trackQLearning;

	// A debug tool so that the cart uses CurrentRoadDirection and not learning.
	public bool useCRDOnly = false;

	// And important property denoting how much time ellapses between a cart's decisions. 
	public float learningRate = 0.2f;

	// Deviation of the learning rate.
	public float learningDeviation = 0.05f;

	/*
	 * ========================================================================================
	 * Private Members
	 * ========================================================================================
	 */

	/*
	 * References to our dependent objects. 
	 */
	private AmbientStateSampler _stateSampler;
	private Cart _cart;

	private CartState _previousCartState;
	private SteeringAction _previousCartAction;

	private Destination _previousOriginDestination;
	private Destination _previousTargetDestination;
	
	private float _currentLearningTime = 0.0f;

	private bool _hasStarted    = false;
	private float _delayToStart = 0.0f;

	/*
	 * Timer to manage when to apply learning again.
	 */
	private float _learningTimer   = 0.0f;
	private float _startDelayTimer = 0.0f;

	/*
	 * ========================================================================================
	 * Private Interface 
	 * ========================================================================================
	 */

	/*
	 * Given a SteeringAction != 1 (which means breaking), calculate the new steering
	 * direction to take.
	 */
	private Vector3 CalculateTargetSteering(SteeringAction action, Vector3 CRD){
		float theta = action.SteeringDirection * Mathf.PI * 2.0f / Configuration.N;

		Quaternion rotation = Quaternion.Euler(0, theta * Mathf.Rad2Deg, 0);
		Matrix4x4 m = Matrix4x4.Rotate(rotation);

		Vector3 steering = m.MultiplyVector(CRD);
		return steering;
	}

	private void ParseAndApplySteeringAction(SteeringAction action, Vector3 CRD){
	    	if (action.SteeringDirection != -1){
	    	        _cart.LetGoBreak();
	    	        _cart.Steering = CalculateTargetSteering(action, CRD);
	    	} else {
	    	        _cart.PushBreak();
	    	}
	}

	private void TakeNewSteertingAction(CartState newState, Vector3 CRD){
		// Ask for learning. If we get a breaking action, then push the 
		// breaks. Otherwise calculate steering and set it to the cart. 
	    	SteeringAction newAction = movementQLearning.Learn(_previousCartState, _previousCartAction, newState);

		ParseAndApplySteeringAction(newAction, CRD);

	    	_previousCartState  = newState;
	    	_previousCartAction = newAction;
	}

	/*
	 * ========================================================================================
	 * Unity Interface 
	 * ========================================================================================
	 */
	void Awake(){ 
		_stateSampler  = GetComponent<AmbientStateSampler>();
		_cart          = GetComponent<Cart>();
		_cart.Steering = _cart.transform.forward;

		_currentLearningTime = learningRate;
		_delayToStart        = UnityEngine.Random.Range(0.0f, 1.0f);

		_previousCartState                        = new CartState();
		_previousCartState.FacingDirection        = 0;
		_previousCartState.NearestObjectDirection = -1;
		_previousCartState.DegreeOfSpeed          = 0;
		_previousCartState.IsRestarting           = false;

		_previousCartAction = new SteeringAction(0);

		_previousOriginDestination = _stateSampler.originDestination;
		_previousTargetDestination = _stateSampler.targetDestination;
	}
	

	/*
	 * This is the main function of this class, and it serves to ask QLearning
	 * to Learn given my previous state and action, and then ask QLearning
	 * for a new best action to take given what we've learned so far. 
	 */
    	void FixedUpdate() {

		/*
		 * Can only make a small amount of decisions per second or
		 * the algorithm will never converge because the action tried
		 * wouldn't last a frame, hence we wouldn't know it's true
		 * effects. 
		 */

		if (_hasStarted == false && _startDelayTimer <= _delayToStart){
			_startDelayTimer += Time.fixedDeltaTime;
		       	return;
		}
		else {
			_hasStarted = true;
		}

		_learningTimer += Time.fixedDeltaTime;

		if (_learningTimer < _currentLearningTime) return;
		else {
			_learningTimer       = 0.0f;
			_currentLearningTime = learningRate + UnityEngine.Random.Range(-learningDeviation, learningDeviation);
		}

		// Acquire Cart's state. 
    	    	(Vector3 CRD, CartState newCartState) sampledState       = _stateSampler.GetState();

		// It might be helpful to compare learning versus purely vectorial 
		// manipulation. Otherwise we just take a steering action.
	    	if (useCRDOnly){
	    	        _cart.Steering = sampledState.CRD;
	    	} else {
			TakeNewSteertingAction(sampledState.newCartState, sampledState.CRD);
		}

	}

	/*
	 * If we hit a Destination trigger, swap the values so that the carts can follow
	 * a new destination.
	 */
	void OnTriggerEnter(Collider other){

		if (other.tag != "Destination") return;

		Destination newDestination = other.GetComponent<Destination>();
		if (newDestination == null){
			throw new NullReferenceException("There's a non-destination game object with that tag.");
		}

		if (newDestination.GetInstanceID() == _previousOriginDestination.GetInstanceID()) return;

		Destination newTargetDestination = trackQLearning.Learn(_previousOriginDestination, _previousTargetDestination, newDestination);

		_stateSampler.targetDestination = newTargetDestination;
		_stateSampler.originDestination = newDestination;

		_previousOriginDestination = newDestination;
		_previousTargetDestination = newTargetDestination;


		// Acquire Cart's state. 
    	    	(Vector3 CRD, CartState newCartState) sampledState       = _stateSampler.GetState();
		TakeNewSteertingAction(sampledState.newCartState, sampledState.CRD);
	}
}
