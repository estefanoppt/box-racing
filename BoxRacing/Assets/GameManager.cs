﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour {

	public bool isGame = false;

	public bool isPlayingWithKeyboard = false;

	public float startTime = 10.0f;

	public CanvasGroup initialGroup;
	public TextMeshProUGUI countDown;

	public TextMeshProUGUI deathmatchTimer;
	public float deathmatchTime = 5.0f * 60.0f;
	private float _deathmathTimer = 0.0f;

	public CanvasGroup finishGroup;
	public TextMeshProUGUI rewardLine;
	public TextMeshProUGUI points;
	public Button retryButton;

	private bool _gamehasEnded = false;


	private List<GameObject> _carts = new List<GameObject>();
	private GameObject       _player = null;
	private float _timer = 0.0f;
	private bool _hasGameStarted = false;

	public Button mainMenuReturn;
	public string mainMenuReturnScene;

	public GameTimeManager timeManager;

	public TextMeshProUGUI hitCounterOutput;
	public TextMeshProUGUI totalRewardAccumulated;
	public TextMeshProUGUI maxRewardOutput;
	public TextMeshProUGUI meanRewardOutput;

	private int _hitCounter;


	public float metricSamplingRate = 0.2f;
	private float _metricTimer      = 0.0f;
	private List<Tuple<float, float>> _metricSamples;

	void Awake(){
		var carts = GameObject.FindGameObjectsWithTag("Cart");
		_carts.AddRange(carts.AsEnumerable());

		foreach (GameObject cart in _carts){
			Cart cartScript = cart.GetComponent<Cart>();
			cartScript.HasCollided += () => _hitCounter++;
		}

		_player = GameObject.FindGameObjectWithTag("Player");

		mainMenuReturn.onClick.AddListener( () => {
					timeManager?.gameObject.SetActive(false);
					Time.timeScale = 1.0f;
					UnityEngine.SceneManagement.SceneManager.LoadScene(mainMenuReturnScene);
				});

		retryButton?.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name) );

		_metricSamples = new List<Tuple<float, float>>((int) (deathmatchTime / metricSamplingRate));
	}

	float hitPerSec = 0.0f;
	float alpha = 0.7f;
	void Update(){
		if (isGame && _gamehasEnded) return;


		if (_hasGameStarted){

			hitPerSec = alpha * hitPerSec + (1.0f - alpha) * (float) _hitCounter / (2.0f * (Time.timeSinceLevelLoad - startTime));
			hitCounterOutput.text = hitPerSec.ToString();

			_metricTimer += Time.deltaTime;
			if (_metricTimer > metricSamplingRate){
				_metricSamples.Add(new Tuple<float, float>(Time.timeSinceLevelLoad - startTime, hitPerSec));
				_metricTimer = 0.0f;
			}
			
			string metricFilePath;
			if (UnityEngine.Input.GetKey(KeyCode.M)){
				// Write metrics to file.
				metricFilePath = UnityEngine.Application.persistentDataPath + "/metrics.csv";

				using (System.IO.StreamWriter file = new System.IO.StreamWriter(metricFilePath)){
					foreach (var sample in _metricSamples){
							file.WriteLine(sample.Item1.ToString() + ", " + sample.Item2.ToString());
					}
				}
			}

			float rewardAccomumulated = 0.0f;
			float maxReward = -Mathf.Infinity;
			foreach (GameObject cartObject in _carts){
				LapCounter counter = cartObject.GetComponent<LapCounter>();
				rewardAccomumulated += counter.RewardCount;

				if (counter.RewardCount > maxReward){
					maxReward = counter.RewardCount;
				}
			}



			totalRewardAccumulated.text = (rewardAccomumulated / Time.timeSinceLevelLoad).ToString("0.00");
			maxRewardOutput.text = maxReward.ToString("0.00");
			meanRewardOutput.text = (rewardAccomumulated / (float)_carts.Count()).ToString("0.00");;

			if (isGame){
				_deathmathTimer += Time.deltaTime * Time.timeScale;

				float remainingTime = deathmatchTime - _deathmathTimer;
				float minutes = Mathf.Floor(remainingTime / 60);
				float seconds = remainingTime - minutes * 60;

				deathmatchTimer.text = minutes.ToString("0") + " [min] " + seconds.ToString("0") + " [sec]";

				if (_deathmathTimer >= deathmatchTime){
					finishGroup.gameObject.SetActive(true);

					LapCounter playerCounter = _player.GetComponent<LapCounter>();
					float playerReward = playerCounter.RewardCount;

					if (playerReward < maxReward){
						rewardLine.text = "You lost!";
					} else {
						rewardLine.text = "You won!";
					}
					points.text = "(with " + playerReward.ToString("0.00") + " [points])";

					_gamehasEnded = true;
					deathmatchTimer.text = "0.00";

					// Write metrics to file.
					metricFilePath = UnityEngine.Application.persistentDataPath + "/metrics.csv";

					using (System.IO.StreamWriter file = new System.IO.StreamWriter(metricFilePath)){
						foreach (var sample in _metricSamples){
							file.WriteLine(sample.Item1.ToString() + ", " + sample.Item2.ToString());
						}
					}
				}
			}

		       return;
		}


		_timer += Time.deltaTime;
		countDown.text = (startTime - _timer).ToString("00.0");

		if (_timer > startTime){
			foreach (GameObject go in _carts){
				Cart cart = go.GetComponent<Cart>();
				cart.enabled = true;

				AmbientStateSampler sampler = go.GetComponent<AmbientStateSampler>();
				sampler.enabled = true;

				QLearnedManipulator manipulator = go.GetComponent<QLearnedManipulator>();
				manipulator.enabled = true;

				LapCounter counter = go.GetComponent<LapCounter>();
				counter.enabled = true;
			}	

			if (_player != null){
				Cart playerCart = _player.GetComponent<Cart>();
				playerCart.enabled = true;

				if (!isPlayingWithKeyboard){
					ControllerManipulator controllerrManipulator = _player.GetComponent<ControllerManipulator>();
					controllerrManipulator.enabled = true;
				} else {
					KeyboardManipulator keyboardManipulator = _player.GetComponent<KeyboardManipulator>();
					keyboardManipulator.enabled = true;
				}

				LapCounter playerCounter = _player.GetComponent<LapCounter>();
				playerCounter.enabled = true;
			}

			_hasGameStarted = true;
			countDown.gameObject.SetActive(false);
			initialGroup?.gameObject.SetActive(false);
		}
	}

}
