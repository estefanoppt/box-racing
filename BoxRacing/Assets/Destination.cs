﻿using UnityEngine;

/*
 * The AmbientStateSampler.CurrentRoadDirection is calculated
 * using the displacement vector between two triggers, where
 * the triggers are placed between road intersections. This
 * class simply represents a road intersection with a pointer
 * to the transform of the next intersection.
 */
using DestinationID = System.Int32;

public class Destination : MonoBehaviour {
	public Destination[] TargetDestinations;
	public DestinationID ID;

	public Transform Min;
	public Transform Max;

	public override int GetHashCode(){
		return transform.position.GetHashCode();
	}
}
