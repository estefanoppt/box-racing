public class Configuration {

	/*
	 * Number of directions that will be sampled by the algorithm. 
	 * For example, if N = 16, then 16 rays will be launched, 
	 * CartState.FacingDirection will be between 0 and 15, and
	 * the learning algorithm will suggest from 16 different
	 * steering directions. 
	 */
	public const sbyte N = 16;

	/*
	 * How many degrees of speed are we prepared to take
	 * into account? A value of 0 means no speed. A value of
	 * 3 means full speed. 
	 */
	public const sbyte DegreesOfSpeed = 3;
}
