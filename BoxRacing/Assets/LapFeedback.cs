﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class LapFeedback : MonoBehaviour {
	public Color baseline;
	public Color excited;
	public float alpha = 0.95f;

	private Color _current;
	private Renderer _renderer;

	void Awake() => _renderer = GetComponent<Renderer>();

	void Start() => _current = baseline;
	void Update(){
		_current = alpha * _current + (1 - alpha) * baseline;
		_renderer.material.color = _current;
	
	}
	
	void OnReward() => _current = excited;
}
