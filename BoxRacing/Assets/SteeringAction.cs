using System;
using System.Runtime.Serialization;

/*
 * An incredibly simple class that contains the direction,
 * relative to CurrentRoadDirection, where the cart is
 * to accelerate to next.
 */
[Serializable]
public struct SteeringAction : ISerializable {
	private sbyte _steeringDirection;
	public sbyte SteeringDirection { 
		get {
			return _steeringDirection;
		}
		private set {
		       _steeringDirection = value;
		}
	}		

	public SteeringAction(sbyte steeringDirection){
		_steeringDirection = steeringDirection;
	}

	public override int GetHashCode(){
		return SteeringDirection.GetHashCode();
	}

	public override string ToString(){
		return _steeringDirection.ToString();
	}

	/*
	 * ========================================================================================
	 * Serializable Interface
	 * ========================================================================================
	 */
	public SteeringAction(SerializationInfo info, StreamingContext context){
		_steeringDirection = (sbyte)info.GetValue("SteeringDirection", typeof(sbyte));
	}

	public void GetObjectData(SerializationInfo info, StreamingContext context){
		info.AddValue("SteeringDirection", SteeringDirection);
	}
}
