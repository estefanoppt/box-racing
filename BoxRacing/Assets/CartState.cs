using System;
using System.Text;

/*
 * This struct represents a state of the Cart according
 * to the model needed by my Q-Learning implementation. 
 * As you'll note, most of the fields are sbytes in order
 * to save memory, because the Q-Learning data-structure
 * (a hash dictionary) can grow unwildly large if we're 
 * not careful, crashing Unity in the process. That's why
 * we don't use Int32 or Int64: because they occupy more memory, 
 * and the expanse of values afforded by these types is not needed
 * in our implementation. 
 */
[Serializable]
public struct CartState {

	/*
	 * Given the CurrentRoadDirection (CRD) (see AmbientStateSampler),
	 * in which of N different directions are we facing? With 0 meaning
	 * that we're facing the CRD, and N being the variable of the same name found in
	 * Configuration.cs.
	 */
	public sbyte FacingDirection;

	// In which if the N different quantized direction is the nearest object's
	// direction, relative to CRD, of the nearest object? -1 means there's no
	// nearest object. 
	public sbyte NearestObjectDirection;

	// What's the velocity's direction, relative to CRD, of the nearest object?
	// -1 means there's not nearest object. 
	public sbyte NearestObjectVelocity;
	
	// What's the degreeOfSpeed of the nearest object? 
	// -1 means there's no nearest object.
	public sbyte NearestObjectDegreeOfSpeed;

	// Are we in imminent collision? (This is defined in AmbientStateSampler). 
	public bool  ImminentCollision;

	// What's our degree of speed? (This is defined in AmbientStateSampler).
	public sbyte DegreeOfSpeed;

	// Are we restarting?
	public bool  IsRestarting;

	/*
	 * ========================================================================================
	 * Equality Comparisons
	 * ========================================================================================
	 */
	/*
	 * public comparison for equality.
	 */
	public override bool Equals(object other){
		CartState? otherCart = other as CartState?;
		if (otherCart == null) return false;

		return this == otherCart.Value;
	}

	public static bool operator==(CartState lhs, CartState rhs){

		bool areEqual = true;
		areEqual = areEqual && (lhs.FacingDirection == rhs.FacingDirection);
		areEqual = areEqual && (lhs.IsRestarting == rhs.IsRestarting);
		areEqual = areEqual && (lhs.DegreeOfSpeed == rhs.DegreeOfSpeed);
		areEqual = areEqual && (lhs.ImminentCollision == rhs.ImminentCollision);
		areEqual = areEqual && (lhs.NearestObjectDirection == rhs.NearestObjectDirection);
		areEqual = areEqual && (lhs.NearestObjectVelocity == rhs.NearestObjectVelocity);
		areEqual = areEqual && (lhs.NearestObjectDegreeOfSpeed == rhs.NearestObjectDegreeOfSpeed);

		return areEqual;
	}

	public static bool operator!=(CartState lhs, CartState rhs){
		return !(lhs == rhs);
	}

	/*
	 * This hash-code generator can be found here:
	 * https://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-overriding-gethashcode
	 *
	 * A good hash-code generator is crucial here since Q-Learning uses an underlying dictionary 
	 * to store the states of the Cart. 
	 */
	public override int GetHashCode(){
		unchecked { // Overflow is fine, just wrap.
			int hash = 17;

			hash = hash * 23 + FacingDirection.GetHashCode();
			hash = hash * 23 + IsRestarting.GetHashCode();
			hash = hash * 23 + ImminentCollision.GetHashCode();
			hash = hash * 23 + DegreeOfSpeed.GetHashCode();
			hash = hash * 23 + NearestObjectDirection.GetHashCode();
			hash = hash * 23 + NearestObjectVelocity.GetHashCode();
			hash = hash * 23 + NearestObjectDegreeOfSpeed.GetHashCode();

			return hash;
		}
	}

	public override string ToString(){
		StringBuilder builder = new StringBuilder();

		builder.Append("FacingDirection: ");
		builder.Append(FacingDirection);

		builder.Append("; IsRestarting: ");
		builder.Append(IsRestarting);

		builder.Append("; ImminentCollision: ");
		builder.Append(ImminentCollision);

		builder.Append("; DegreeOfSpeed: ");
		builder.Append(DegreeOfSpeed);

		builder.Append("; NearestObjectDirection: ");
		builder.Append(NearestObjectDirection);

		builder.Append("; NearestObjectVelocity: ");
		builder.Append(NearestObjectVelocity);

		builder.Append("; NearestObjectDegreeOfSpeed: ");
		builder.Append(NearestObjectDegreeOfSpeed);

		return builder.ToString();
	}


}
