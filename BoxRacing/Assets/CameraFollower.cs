﻿using UnityEngine;

public class CameraFollower : MonoBehaviour {

	public Transform player;
	public Vector3 offsetFromPlayer;

	void Update(){
		Vector3 playerFacing = player.forward;

		float theta = Mathf.Atan2(playerFacing.x, playerFacing.z);

		Quaternion q              = Quaternion.Euler(0.0f, theta * Mathf.Rad2Deg, 0.0f);
		Matrix4x4 m               = Matrix4x4.Rotate(q);
		Vector3 transformedOffset = m.MultiplyPoint(offsetFromPlayer);
		transform.position        = player.position + transformedOffset;

		transform.LookAt(player, Vector3.up);
	}

}
