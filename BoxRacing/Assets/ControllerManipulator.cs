﻿using UnityEngine;

/*
 * A placeholder to test the feel of the cart using a controller. 
 * The idea is to program a QLearnedManipulator that replaces this
 * human-driven input by a machine-learned input. 
 */
[RequireComponent(typeof(Cart))]
public class ControllerManipulator : MonoBehaviour {
	
	public Cart toControl;

	void Awake(){
		if (toControl == null)
			throw new System.NullReferenceException("ToControl has be set to null.");
	}

    	void Update() {
		float XAxis = Input.GetAxis("Horizontal");
		float YAxis = Input.GetAxis("Vertical");

		Vector3 direction  = new Vector3(XAxis, 0.0f, YAxis);
		Debug.Log(XAxis + "," + YAxis);
		toControl.Steering = direction.normalized;

		float trigger = Input.GetAxis("Break");
		if (trigger > 0.75f){
			toControl.PushBreak();
		} else {
			toControl.LetGoBreak();
		}
    	}
}
