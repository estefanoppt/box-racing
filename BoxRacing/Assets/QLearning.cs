using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;


/*
 * This is the main class of the game. It implements Q-Learning through the
 * use of a Dictionary. The algorithm is a modified version of the algorithm
 * in Ian Millongton's AI for Games, pg. 627. 
 */

using QValue = System.Single;
using Reward = System.Single;

namespace Freaky {
	public class QLearning<State, Action> {

		/*
		 * ========================================================================================
		 * Public Members
		 * ========================================================================================
		 */
		private float _alpha;
		public float Alpha { 
			get { return _alpha; } 
			set {
		       		if ( value >= 0.0f && value <= 1.0f )
					_alpha = value;
				else throw new ArgumentException("Alpha was set to a value not in [0, 1].");
			}
		}

		private float _gamma;
		public float Gamma { 
			get { return _gamma; } 
			set {
		       		if ( value >= 0.0f && value <= 1.0f )
					_gamma = value;
				else throw new ArgumentException("Gamma was set to a value not in [0, 1].");
			}
		}

		private float _rho;
		public float Rho { 
			get { return _rho; } 
			set {
		       		if ( value >= 0.0f && value <= 1.0f )
					_rho = value;
				else throw new ArgumentException("Gamma was set to a value not in [0, 1].");
			}
		}

		public bool DebugEnabled { get; set; }

		/*
		 * The main data structure which saves the quality values for each of the 
		 * transitions. 
		 */
		public Dictionary<State, Dictionary<Action, QValue>> QValues { get; set; }

		/*
		 * ========================================================================================
		 * Private Members
		 * ========================================================================================
		 */

		private Func<State, Reward> GetReward { get; set; }

		/*
		 * ========================================================================================
		 * Private Interface
		 * ========================================================================================
		 */
		/*
		 * We traverse the Dictionary looking for the best action possible given the current
		 * state. Notice that this isn't your typical "find max" function. It chooses, 
		 * at random, from different sets of actions that produce the same results. This
		 * helps avoid bias, and let true learning happen. 
		 */
		private Action GetBestAction(State state){

			List<Action> bestActions = new List<Action>();
			var allActions = QValues[state].Keys;

			QValue maxQ = -Mathf.Infinity;

			foreach (Action action in allActions){
				QValue Q = QValues[state][action];
				if (Q > maxQ){
					maxQ = Q;
					bestActions.Clear();
				}
				if (Q == maxQ){
					bestActions.Add(action);
				}
			}

			return bestActions[UnityEngine.Random.Range(0, bestActions.Count)];
		}

		/*
		 * We just get a random action. 
		 */
		private Action GetRandomAction(State state){
			var allActions = QValues[state].Keys;
			return allActions.OrderBy( (action) => UnityEngine.Random.Range(0.0f, 1.0f) ).First();
		}

		/*
		 * ========================================================================================
		 * Constructors
		 * ========================================================================================
		 */
		public QLearning(float alpha, float gamma, float rho, Func<State, Reward> getReward, Action<Dictionary<State, Dictionary<Action, QValue>>> initStates){
			Alpha     = alpha;
			Gamma     = gamma;
			Rho       = rho;
			GetReward = getReward;

		        QValues = new Dictionary<State, Dictionary<Action, QValue>>();
			initStates(QValues);
		}

		/*
		 * ========================================================================================
		 * Public Interface
		 * ========================================================================================
		 */
		/*
		 * This is the main function of QLearning. It learns and then suggests a new action
		 * to the Agent based on the previous state, the previous action, and the new state. 
		 * This is based on Ian Millington's implementation, but it's modified so that it's 
		 * online learning as opposed to offline learning, and it also doesn't query the new
		 * state from a pre-built model, but the new state is sampled from the world 
		 * by AmbientStateSampler. 
		 */
		public Action Learn(State previousState, Action previousAction, State newState){

			// Sometimes a random action allows us to learn novel pathways. 
			Action newAction;
			if (UnityEngine.Random.Range(0.0f, 1.0f) < Rho) newAction = GetRandomAction(newState);
			else newAction = GetBestAction(newState);

			// Get the reward for the current state if there is one. 
			float reward = GetReward(newState);

			// Get quality values. 
			QValue Q    = QValues[previousState][previousAction];
			QValue maxQ = QValues[newState][newAction];

			// The main learning function. Can be found in Ian Millington's AI for Games, 
			// page 629. 
			Q = QValues[previousState][previousAction] = (1 - Alpha) * Q + Alpha * (reward + Gamma * maxQ);

			if (DebugEnabled){
				Debug.Log("-----------------------------------------------------------------------");
				Debug.Log("PreviousState: "  + previousState.ToString());
				Debug.Log("PreviousAction: " + previousAction.ToString());
				Debug.Log("NewState: "       + newState.ToString());
				Debug.Log("NewAction: "      + newAction.ToString());
				Debug.Log(Q.ToString()       + ", " + maxQ.ToString());
			}

			return newAction;
		}

		/*
		 * CAUTION! This will take an enourmous amount of time to run! Return size of QLearning object
		 * in bytes. This method of calculating an object's size was taken from:
		 * 	https://stackoverflow.com/questions/605621/how-to-get-object-size-in-memory
		 * Use with caution.
		 */
		public long GetSize(){
			using (Stream s = new MemoryStream()) {
				BinaryFormatter formatter = new BinaryFormatter();
				formatter.Serialize(s, QValues);
				return s.Length;
			}
		}


	}
}
