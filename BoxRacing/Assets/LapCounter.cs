using UnityEngine;
using TMPro;

using System.Collections.Generic;
using System.Collections;

public class LapCounter : MonoBehaviour {

	/*
	 * ========================================================================================
	 * Unity Interface Variables
	 * ========================================================================================
	 */
	public TextMeshPro rewardCounterDisplay;
	public float lossRate = 0.01f;
	public int winPerLap = 3;
	public int lossPerHit = 2;
	public Transform lapLight;
	public AudioSource rewardSource;
	public AudioSource punishmentSource;

	private Cart _cart;
	
	/*
	 * ========================================================================================
	 * Private Members
	 * ========================================================================================
	 */
	private Camera _camera;
	private HashSet<Destination> _allDestinations; 
	private HashSet<Destination> _seenDestinations;

	void Awake(){
		_camera = GameObject.FindObjectOfType<Camera>();
		_cart   = GetComponent<Cart>();
		_cart.HasCollided += OnHit;

		/*
		 * We must find all destinations and add them to the hash set. 
		 */
		Destination[] allDestinations = GameObject.FindObjectsOfType<Destination>();
		_allDestinations = new HashSet<Destination>(allDestinations);
		_seenDestinations = new HashSet<Destination>();
	}

	/*
	 * =========================================================================================
	 * Public Interface
	 * =========================================================================================
	 */
	private float _rewardCount = 0;
	public float RewardCount { 
		get{ return _rewardCount;}
	       
		private set {
			_rewardCount = value;
		}
	}
	/*
	 * ========================================================================================
	 * Unity Interface 
	 * ========================================================================================
	 */
	void Update(){
		rewardCounterDisplay.SetText(_rewardCount.ToString("0.00"));
		rewardCounterDisplay.transform.LookAt(_camera.transform);
	}

	void FixedUpdate(){
		_rewardCount -= lossRate * Time.fixedDeltaTime;
	}

	private IEnumerator _coroutine;
	void OnTriggerEnter(Collider other){
		if (other.tag != "Destination") return;

		Destination current = other.GetComponent<Destination>();
		if (!_seenDestinations.Contains(current)){
			_rewardCount += winPerLap;
			rewardSource.Play();
			_seenDestinations.Add(current);
			current.SendMessage("OnReward");
			_coroutine = lightingFeedback();
			StartCoroutine( _coroutine );
		}

		if (_allDestinations.SetEquals(_seenDestinations)){
			_seenDestinations.Clear();
		}
	}

	private IEnumerator lightingFeedback(){
		lapLight.gameObject.SetActive(true);
		yield return new WaitForSeconds(1.0f);
		lapLight.gameObject.SetActive(false);
	}


	void OnHit(){
		if (!_cart.IsRestarting)
			_rewardCount -= lossPerHit;

		punishmentSource.Play();
	}

}
