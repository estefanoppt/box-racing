using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour {

	public Button race;
	public string raceScene;

	public Button theme;
	public string themeScene;

	public Button watchPreLoaded;
	public string watchPreLoadedScene;

	public Button watchScratch;
	public string watchScratchScene;


	void Awake(){
		race.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene(raceScene) );
		theme.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene(themeScene) );
		watchPreLoaded.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene(watchPreLoadedScene) );
		watchScratch.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene(watchScratchScene) );
	}

}
