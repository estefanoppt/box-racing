using UnityEngine;

/*
 * A placeholder to test the feel of the cart using a controller. 
 * The idea is to program a QLearnedManipulator that replaces this
 * human-driven input by a machine-learned input. 
 */
[RequireComponent(typeof(Cart))]
public class KeyboardManipulator : MonoBehaviour {
	
	public Cart toControl;
	public Camera camera;	

	void Awake(){
		if (toControl == null)
			throw new System.NullReferenceException("ToControl has be set to null.");
	}

    	void Update() {

		Vector3 elementPos = camera.WorldToScreenPoint(toControl.transform.position);

		float mouseX = Input.mousePosition.x;
		float mouseY = Input.mousePosition.y;
		Vector3 mousePos = new Vector3(mouseX, mouseY, 0.0f);

		Vector3 displacement = Vector3.Normalize(mousePos - elementPos);

		Debug.Log(mouseX + " " + mouseY);

		if (Input.GetMouseButton(0)){
			toControl.Steering = Vector3.Normalize(new Vector3(displacement.x, 0.0f, displacement.y));
		} else {
			toControl.Steering = Vector3.zero;
		}

		if (Input.GetMouseButton(1)){
			toControl.PushBreak();
		} else {
			toControl.LetGoBreak();
		}
	}
}

