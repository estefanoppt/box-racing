using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/*
 * This is an ad-hic class that initializes the graph of the Maze 
 * scene for this project. 
 */
public class GraphInitializer : MonoBehaviour {

	private const int SAMPLES = 4;

	public float sampleDistance = 10.0f;

	private Dictionary<Destination, List<Destination>> _mesh = new Dictionary<Destination, List<Destination>>();

	void Awake(){

		// Each tracker will sample in the four cardinal directions. 
		Vector3[] sampleDirections = new Vector3[SAMPLES];
		sampleDirections[0]        = Vector3.forward;
		sampleDirections[1]        = Vector3.right;
		sampleDirections[2]        = Vector3.back;
		sampleDirections[3]        = Vector3.left;

		// We need to consider only Destination objects. 
		int layerMask = 1 << LayerMask.NameToLayer("Destination");

		// Get all tracker-tagged objects. 
		var allTrackers = GameObject.FindGameObjectsWithTag("Tracker");


		// For every tracker node. 
		foreach (var node in allTrackers){

			/*
			 * We launch a ray for each direction, and save on a list
			 * the Destination that where closest to the hit.
			 */
			List<Destination> hitDestinations = new List<Destination>();
			foreach (var direction in sampleDirections){
				RaycastHit[] hits = Physics.RaycastAll(
								node.transform.position, 
								direction,
								sampleDistance, 
								layerMask,
								QueryTriggerInteraction.Collide
							);

				if (hits.Length > 0){
					RaycastHit closestHit = hits.Aggregate( 
						(closest, next) => {
							return closest.distance > next.distance ? next : closest;
						}
					);

					Destination found = closestHit.transform.GetComponent<Destination>();
					hitDestinations.Add(found);
				}
			}


			/*
			 * We build the graph by using the dictionary and adding
			 * all connections through use of a list.
			 */
			foreach (var parentDestination in hitDestinations){
				if (!_mesh.ContainsKey(parentDestination)){
					_mesh[parentDestination] = new List<Destination>();
				}

				foreach (var childDestination in hitDestinations){
					if (parentDestination == childDestination) continue;
					_mesh[parentDestination].Add(childDestination);
				}
			}
		}

		/*
		 * Once the mesh is built, we proceed to create the actual graph
		 * through the Destination objects.
		 */
		foreach (var parentDestination in _mesh.Keys){

			// If user has set the target destinations, skip this step. 
			if (parentDestination.TargetDestinations.Length > 0) continue;

			parentDestination.TargetDestinations = new Destination[_mesh[parentDestination].Count()];
			for (int i = 0; i < parentDestination.TargetDestinations.Length; i++){
				parentDestination.TargetDestinations[i] = _mesh[parentDestination][i];
				Debug.DrawLine(parentDestination.transform.position, _mesh[parentDestination][i].transform.position, Color.cyan, 1.0f);
			}
		}
	}
}
