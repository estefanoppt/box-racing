using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]

/*
 * This script automatically creates the Min and Max transforms
 * needed by the new planning algorithm to calculate the current
 * road value in AmbientStateSampler. It assumes all Destinations
 * are AABBs.
 */
public class MazeInitializer : MonoBehaviour {
	void OnEnable(){
		Destination[] allDestinations = GameObject.FindObjectsOfType<Destination>();

		foreach (Destination destination in allDestinations){

			// We remove all children first;
			List<Transform> allChildren = new List<Transform>();
			for (int i = 0; i < destination.transform.childCount; i++){
				allChildren.Add(destination.transform.GetChild(i));
			}

			destination.transform.DetachChildren();

			foreach (var child in allChildren){
				Object.DestroyImmediate(child.gameObject);
			}

			allChildren.Clear();
			

			// Now we re-create the min and max values.
			BoxCollider collider = destination.GetComponent<BoxCollider>();
			Bounds bounds        = collider.bounds;

			GameObject min = new GameObject("Min");
			min.transform.SetParent(destination.transform);
			min.transform.position = bounds.min;
			destination.Min        = min.transform;

			GameObject max = new GameObject("Max");
			max.transform.SetParent(destination.transform);
			max.transform.position = bounds.max;
			destination.Max        = max.transform;
		}
	}
}
