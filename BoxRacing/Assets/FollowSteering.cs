﻿using UnityEngine;

/*
 * Used so that there's feedback for the current acceleration of the cart. 
 */
[RequireComponent(typeof(Cart))]
public class FollowSteering : MonoBehaviour {

	private float _accumTheta = 0.0f;

	[Range(0.0f, 1.0f)]
	public float alpha        = 0.875f;
	public Cart ToFollow;

	void Start() {
		if (ToFollow == null)
			throw new System.NullReferenceException("ToFollow has be set to null.");
    	}

	private float _previousTheta = 0.0f;
    	void Update() {
		float theta;
		if (ToFollow.Steering.magnitude == 0.0f){
			theta = _previousTheta;
		} else {
			theta = Mathf.Atan2(ToFollow.Steering.x, ToFollow.Steering.z);
			_previousTheta = theta;
		}

		_accumTheta = alpha * _accumTheta + (1 - alpha) * theta;

		transform.rotation = Quaternion.Euler(0, _accumTheta * Mathf.Rad2Deg, 0);
    	}
}
