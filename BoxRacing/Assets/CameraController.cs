using UnityEngine;

public class CameraController : MonoBehaviour {

	public float XRotationAcceleration = 1.0f;
	public float YRotationAcceleration = 1.0f;
	public float ZRotationAcceleration = 1.0f;
	public float ForwardAcceleration   = 1.0f;
	public float TurboScalingFactor    = 2.0f;

	private float _yaw = 0.0f;


	void Update(){
		float viewX = Input.GetAxis("HorizontalView");
		float viewY = Input.GetAxis("VerticalView");

		if (Input.GetAxis("YawLeft") > 0.75) _yaw  = -1;
		if (Input.GetAxis("YawRight") > 0.75) _yaw = +1;

		transform.Rotate(viewY*YRotationAcceleration*Time.deltaTime, viewX*XRotationAcceleration*Time.deltaTime, _yaw*ZRotationAcceleration *Time.deltaTime, Space.Self);

		_yaw = 0;


		float turbo = 1.0f;
		if (Input.GetAxis("Turbo") > 0.75f){
			turbo = TurboScalingFactor;
		}


		float XAxis = Input.GetAxis("Horizontal");
		float YAxis = Input.GetAxis("Vertical");

		Vector3 axisDirection = new Vector3(XAxis, 0, YAxis);
		Vector3 transformedDirection = axisDirection;

		if (Input.GetAxis("LiftDown") > 0.75f) transformedDirection.y = -1.0f;
		if (Input.GetAxis("LiftUp")   > 0.75f) transformedDirection.y = +1.0f;

		transformedDirection.Normalize();

		transform.Translate(transformedDirection * turbo * ForwardAcceleration * Time.deltaTime);



	}
}
