﻿using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;

using Freaky;
using QValue = System.Single;

public class MovementQLearning : MonoBehaviour {
	/*
	 * ========================================================================================
	 * Unity Interface Variables
	 * ========================================================================================
	 */

	/*
	 * These are the parameters of the Q-Learning algoritm. 
	 * 	1. alpha is the learning rate. 
	 * 	2. gamma is the discount rate. 
	 * 	3. Rho is the randomness for exploration.
	 */
	[Range(0.0f, 1.0f)]
	public float alpha = 0.3f;

	[Range(0.0f, 1.0f)]
	public float gamma = 0.75f;

	[Range(0.0f, 1.0f)]
	public float rho   = 0.2f;
	
	// Rewards and punishments. 
	public AnimationCurve rewardsCurve;
	public AnimationCurve speedDampingCurve;
	public float restartingPunishment = -10.0f;

	// Debug
	public bool debug = false;

	public float serializationCooldown = 2.0f;

	// Start without any training.
	public bool startFromScratch = false;

	/*
	 * ========================================================================================
	 * Private Members
	 * ========================================================================================
	 */

	// An instance of the QLearning algorithm.
	private QLearning<CartState, SteeringAction> _qLearning;

	/*
	 * ========================================================================================
	 * Delegates
	 * ========================================================================================
	 */
	/*
	 * We have hardcoded the delivery of rewards for this problem. If the Cart 
	 * has collided, then that transition gets a punishment of -1.0f. If there
	 * are objects nearby, but we haven't collided, that doesn't generate any
	 * reward. If there are no objects nearby, we reward according to how 
	 * much we're facing the CurrentRoadDirection. The design of the 
	 * rewardsCurve AnimationCurve is essential for a proper management of
	 * the rewards. Remember that a state.FacingDirection of 0 means the
	 * Cart is facing the CurrentRoadDirection, so the reward ought to be
	 * the maximum. Likewise, if the state.FacingDirection is 15 (in the case
	 * that Configuration.N is 16), then the reward is almost maximum as well.
	 */
	private float GetReward(CartState state){
		if (state.IsRestarting) return restartingPunishment;

		float speedDamping = speedDampingCurve.Evaluate( ((float) state.DegreeOfSpeed) / (float) Configuration.DegreesOfSpeed );
		return speedDamping * rewardsCurve.Evaluate( ((float) state.FacingDirection) / (float) Configuration.N);
	}

	/*
	 * We must provide a means to initialize the QValue store to all possible states. 
	 */
	private void InitState(Dictionary<CartState, Dictionary<SteeringAction, QValue>> QValues){
		// Initialize all the action available to the agent. ------------------------------
		SteeringAction[] SteeringActions = new SteeringAction[Configuration.N + 1];
		/*
		 * We've got Configuration.N different steering actions. Every state has every one
		 * of this actions available. The SteeringActions are relative to the 
		 * AmbientStateSampler.CurrentRoadDirection. We add one to account for breaking.
		 */
		for (sbyte i = 0; i < SteeringActions.Length - 1; i++){
			SteeringActions[i] = new SteeringAction(i);
		}
		
		// Accound for breaking;
		SteeringActions[SteeringActions.Length - 1] = new SteeringAction(-1);

		// Initialize QValues to 0 everywhere. ----------------------------------------
		// The set of all possible collision values.
		bool[] isRestartingPosibilities = new bool[2]{false, true};
		bool[] ImminentCollisionPossibilities = new bool[2]{ false, true };

		/*
		 * For each FacingDirection, for each NearyObjectDirection, for each
		 * ImminentCollision alternative, for each DegreeOfSpeed, and for each
		 * IsCollided alternative, initialize the QValue store to 0.0f.
		 */
		for (sbyte i = 0; i < Configuration.N; i++)
		for (sbyte j = -1; j < Configuration.N; j++)
		for (sbyte l = -1; l < Configuration.N; l++)
		for (sbyte h = -1; h <= Configuration.DegreesOfSpeed; h++)
		for (sbyte m = 0; m <= Configuration.DegreesOfSpeed; m++)
		for (int n = 0; n < ImminentCollisionPossibilities.Length; n++)
		for (int k = 0; k < isRestartingPosibilities.Length; k++){
			CartState current                  = new CartState();
			current.FacingDirection            = i;
			current.NearestObjectDirection     = j;
			current.NearestObjectVelocity      = l;
			current.NearestObjectDegreeOfSpeed = h;
			current.ImminentCollision          = ImminentCollisionPossibilities[n];
			current.DegreeOfSpeed              = m;
			current.IsRestarting               = isRestartingPosibilities[k];

			Dictionary<SteeringAction, QValue> actionQValues = new Dictionary<SteeringAction, QValue>();
			for (int q = 0; q < SteeringActions.Length; q++){
				actionQValues.Add(SteeringActions[q], 0.0f);
			}
			QValues.Add(current, actionQValues);
		}
	}

	/*
	 * ========================================================================================
	 * Public Interface
	 * ========================================================================================
	 */
	public SteeringAction Learn(CartState previousState, SteeringAction previousAction, CartState newState) 
		=> _qLearning.Learn(previousState, previousAction, newState);

	/*
	 * ========================================================================================
	 * Unity Interface
	 * ========================================================================================
	 */

	/*
	 * On Awake() we move the pre-installed movement QValues onto disk.
	 */
	void Awake(){
		_serializationPath = Application.persistentDataPath + "/MovementQValues.bytes";

		if (!File.Exists(_serializationPath)){
			TextAsset qValuesBinary = Resources.Load<TextAsset>("MovementQValues");

			if (qValuesBinary != null){
				FileStream fs = new FileStream(_serializationPath, FileMode.Create);

				byte[] bytes = qValuesBinary.bytes;
				fs.Write(bytes, 0, bytes.Length);
				fs.Close();
			} 
		}
	}

	/*
	 * We quite simply keep a buffer of all state transtions available, 
	 * which are one of the 16 directions available to the Carts. 
	 *
	 * We also initialize the entire QValues dictionary, which is a long 
	 * proceedure.
	 */
	void Start(){
		_qLearning         = new QLearning<CartState, SteeringAction>(alpha, gamma, rho, GetReward, InitState);

		// We start by loading the stored Q-Values.
		if (File.Exists(_serializationPath) && !startFromScratch){
			_loadQStore = Task.Factory.StartNew( () => { return LoadQValues(_serializationPath); } );
			_serializationEnabled = false;
		}
	}

	/*
	 * On Update() we serialize the game object. We provide a cooldown so
	 * that the user has to wait a couple of seconds before doing the action
	 * again (we don't want this executing once per frame). 
	 */
	private float _timer               = 0.0f;
	private bool _serializationEnabled = true;
	private string _serializationPath;
	private Task<Dictionary<CartState, Dictionary<SteeringAction, QValue>>> _loadQStore = null;

	private void Update(){

		if (Input.GetKeyDown(UnityEngine.KeyCode.P) && _serializationEnabled){
			_serializationEnabled = false;
			Task.Run(() => SaveQValues(_serializationPath, _qLearning.QValues));
		}

		if (Input.GetKeyDown(KeyCode.L) && _serializationEnabled){
			_serializationEnabled = false;
			_loadQStore = Task.Factory.StartNew( () => { return LoadQValues(_serializationPath); } );
		}

		if (_loadQStore != null && _loadQStore.IsCompleted){
			_qLearning.QValues = _loadQStore.Result;
			_loadQStore = null;
		}

		if (_serializationEnabled == false){
			_timer += Time.deltaTime;
			if (_timer > serializationCooldown){
				_serializationEnabled = true;
				_timer = 0.0f;
			}
		}

		/*
		 * We do this so that we can modify the algorithm's 
		 * behavior in real time.
		 */
		_qLearning.Alpha        = alpha;
		_qLearning.Gamma        = gamma;
		_qLearning.Rho          = rho;
		_qLearning.DebugEnabled = debug;

	}

	/*
	 * ========================================================================================
	 * Serializing Methods
	 * ========================================================================================
	 */
	private void SaveQValues(string serializationPath, Dictionary<CartState, Dictionary<SteeringAction, QValue>> qValueStore){
		Debug.Log("SaveQValues: Beginning");

		FileStream fs         = new FileStream(serializationPath, FileMode.Create);

		unchecked {
			foreach (var qValuePair in qValueStore){
				CartState state = qValuePair.Key;
				Dictionary<SteeringAction, QValue> actions = qValuePair.Value;

				fs.WriteByte((byte) state.FacingDirection);
				fs.WriteByte((byte) state.NearestObjectDirection);
				fs.WriteByte((byte) state.NearestObjectVelocity);
				fs.WriteByte((byte) state.NearestObjectDegreeOfSpeed);
				fs.WriteByte((byte) state.DegreeOfSpeed);
				fs.WriteByte(System.BitConverter.GetBytes(state.ImminentCollision)[0]);
				fs.WriteByte(System.BitConverter.GetBytes(state.IsRestarting)[0]);

				foreach (var actionPair in actions){
					fs.WriteByte((byte) actionPair.Key.SteeringDirection);

					QValue Q      = actionPair.Value;
					byte[] qBytes = System.BitConverter.GetBytes(Q);
					fs.WriteByte(qBytes[0]);
					fs.WriteByte(qBytes[1]);
					fs.WriteByte(qBytes[2]);
					fs.WriteByte(qBytes[3]);
				}
			}
		}

		fs.Close();
		Debug.Log("SaveQValues: Ending");
	}

	private Dictionary<CartState, Dictionary<SteeringAction, QValue>> LoadQValues(string serializationPath){
		Debug.Log("LoadQValues: Beginning");

		FileStream fs           = new FileStream(serializationPath, FileMode.Open);
		Dictionary<CartState, Dictionary<SteeringAction, QValue>> qValueStore = new Dictionary<CartState, Dictionary<SteeringAction, QValue>>();

		unchecked {
			int readByte = fs.ReadByte();
			do {
				CartState state                  = new CartState();
				state.FacingDirection            = (sbyte) readByte;
				state.NearestObjectDirection     = (sbyte) fs.ReadByte();
				state.NearestObjectVelocity      = (sbyte) fs.ReadByte();
				state.NearestObjectDegreeOfSpeed = (sbyte) fs.ReadByte();
				state.DegreeOfSpeed              = (sbyte) fs.ReadByte();
				state.ImminentCollision          = fs.ReadByte() == 1 ? true : false;
				state.IsRestarting               = fs.ReadByte() == 1 ? true : false;

				Dictionary<SteeringAction, QValue> actions = new Dictionary<SteeringAction, QValue>();
				byte[] QBytesArray = new byte[4];
				for (int i = -1; i < Configuration.N; i++){
					sbyte direction = (sbyte) fs.ReadByte();
					SteeringAction action = new SteeringAction(direction);

					fs.Read(QBytesArray, 0, 4);
					QValue qValue = System.BitConverter.ToSingle(QBytesArray, 0);

					actions.Add(action, qValue); 
				}

				qValueStore.Add(state, actions);

				readByte = fs.ReadByte();
			} while (readByte != -1);
		}
		fs.Close();

		Debug.Log("LoadQValues: Ending");
		return qValueStore;
	}

}


