using UnityEngine;
using System;

/*
 * This is a simple Component which applies an acceleration
 * set through the Steering property, and a breaking mechanism
 * by modifying the drag of the Rigibody. It also has a Restarting
 * state. This means that the cart "shuts itself down" when it's 
 * hit by an obstacle, and must wait a re-start time before moving 
 * again. 
 */

[RequireComponent(typeof(Rigidbody), typeof(Renderer))]
public class Cart : MonoBehaviour {

	/*
	 * ========================================================================================
	 * Public Properties
	 * ========================================================================================
	 */
	/*
	 * The GameObject's Rigidbody will follow this vector's direction. Note that this
	 * is not an acceleration vector, but a direction.
	 */
	private Vector3 _steering = Vector3.zero;
	public Vector3 Steering { 
		get { return _steering;} 
	       	set { 
			_steering = value;
			_steering.Normalize();
	       	}
	}

	public float MaxSpeedReached { get; private set; }

	/*
	 * Whether the Cart is waiting to restart after a collision.
	 */
	public bool IsRestarting { get; private set; }

	/*
	 * ========================================================================================
	 * Unity Interface Variables
	 * ========================================================================================
	 */

	public bool isPlayer = false;
	public float acceleration;
	public float drag = 0.1f;

	// The drag to apply to the RigidBody during a PushBreak() action. 
	public float breakDrag = 10;

	// The drag to apply to the RigidBody during normal acceleration of the Cart. 
	public float accelerateDrag = 1;

	// The drag to apply to the RigidBody during restarting of the Cart. 
	public float restartingDrag = 1;

	// Color of the Cart while breaking. 
	public Color breakColor;

	// Color of the Cart while accelerating. 
	public Color accelerateColor;

	// Color of the Cart while colliding and restarting. 
	public Color collidedColor;

	// Time before the Cart can restart movement after a collision. 
	public float restartingTime = 1.0f;

	// The ratio by which the color will be changed from frame to frame (see below). 
	[Range(0.0f, 1.0f)]
	public float colorAlpha = 0.3f;

	// The ratio by which the rotation will be changed from frame to frame (see below).
	[Range(0.0f, 1.0f)]
	public float rotationAlpha = 0.7f;

	public Transform hitLight;
	/*
	 * ========================================================================================
	 * Public Interface
	 * ========================================================================================
	 */

	public event Action HasCollided;

	/*
	 * Hits the break on the Cart, changing the RigidBody's drag, effectively halting 
	 * the cart's movement.
	 */
	public void PushBreak(){
		if (IsRestarting) return;

		_rigidbody.drag = breakDrag;
		_targetColor    = breakColor;
	}
	
	/*
	 * Release the breaks of the Cart, restoring the normal drag of the cart whilist the
	 * cart is driving. 
	 */
	public void LetGoBreak(){
		if (IsRestarting) return;

		_rigidbody.drag = accelerateDrag;
		_targetColor    = accelerateColor;
	}

	/*
	 * ========================================================================================
	 * Private Members
	 * ========================================================================================
	 */
	private Rigidbody _rigidbody;
	private Renderer _renderer;

	/*
	 * This is the current target color for the Cart's feedback. Remember that the cart
	 * can be accelerating, breaking, or re-starting. We change this private variable
	 * to signal a change in feedback. 
	 */
	private Color _targetColor;
	/*
	 * ========================================================================================
	 * Unity Interface
	 * ========================================================================================
	 */

	/*
	 * Awake() is called once per GameObject. This is called during the initialization
	 * of the GameObject, before the first frame of the game is played. 
	 */
	void Awake(){ 
		Steering       = Vector3.zero;
		IsRestarting   = false;
		_rigidbody     = GetComponent<Rigidbody>();
		_renderer      = GetComponent<Renderer>();
		_targetColor   = accelerateColor;
	}

	/*
	 * FixedUpdate() is called a fixed number of times per frame. All physics calculations
	 * should go in here, since it's not dependent on the rendering Update(), which can 
	 * vary depending on the specific circumstances of the computer aat any given moment.
	 */
	private float _timer     = 0.0f;
	private float _lastTheta = 0;
    	void FixedUpdate() {

		// Logic for when Cart is restarting -----------------------------------------------
		if (IsRestarting && _timer < restartingTime){
			_timer += Time.fixedDeltaTime;
		}
		if (IsRestarting && _timer >= restartingTime){
			IsRestarting         = false;
			_timer               = 0.0f;
			_rigidbody.drag      = accelerateDrag;
			_targetColor         = accelerateColor;
			hitLight.gameObject.SetActive(false);
		}
		if (IsRestarting) return;

		// Logic for Cart's movement -------------------------------------------------------
		if (!isPlayer){
			_rigidbody.AddForce(acceleration * Steering * Time.fixedDeltaTime);
		} else {
			_rigidbody.velocity +=  acceleration * Steering * Time.fixedDeltaTime;
			_rigidbody.velocity -= _rigidbody.velocity * Mathf.Pow(drag, 1.0f);
		}
		
		// Logic for Cart's rotation -------------------------------------------------------
		float theta      = 0.0f;
		
		if (_rigidbody.velocity == Vector3.zero){
			// If the velocity is the zero vector, keep pointing at the same direction as the last frame.
			theta = _lastTheta;
		} else {
			// Rotate according to rigidbody velocity.
			theta      = Mathf.Atan2(_rigidbody.velocity.x, _rigidbody.velocity.z);
		}
		transform.rotation = Quaternion.Euler(0, theta * Mathf.Rad2Deg, 0);
		_lastTheta = theta;

		MaxSpeedReached = (MaxSpeedReached < _rigidbody.velocity.magnitude ? _rigidbody.velocity.magnitude : MaxSpeedReached);
    	}

	/*
	 * This is the rendering Update(), which is called per every frame rendered. In here we use a simple
	 * exponential function that acts as a weighed average of the target color and the current color of the 
	 * cart. This allows us to change the _targetColor elsewhere and this function will simply drive to
	 * converge to that color smoothly, liberating us from having to use the _renderer in other parts
	 * of the script. 
	 */
	void Update(){
		_renderer.material.color = colorAlpha * _targetColor + (1 - colorAlpha) * _renderer.material.color;
	}

	/*
	 * If we collide with anything, then the Cart goes into the start Restarting.
	 */
	void OnCollisionEnter() {
		HasCollided?.Invoke();
		_targetColor    = collidedColor;
		IsRestarting    = true;
		_rigidbody.drag = restartingDrag;
		hitLight.gameObject.SetActive(true);
	}
}
