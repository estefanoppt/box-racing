using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Freaky;
using QValue = System.Single;
using DestinationID = System.Int32;

/*
 * This class represents a behavior of using Q-Learning to learn
 * the fastest route through a track. The internal algorithm, 
 * QLearning, uses System.Int32, the identifiers of each destination.
 * But this class presets, outwardly, an interface based on actual 
 * Destination references. 
 */

public class TrackQLearning : MonoBehaviour {
	/*
	 * ========================================================================================
	 * Unity Interface Variables
	 * ========================================================================================
	 */

	/*
	 * These are the parameters of the Q-Learning algoritm. 
	 * 	1. alpha is the learning rate. 
	 * 	2. gamma is the discount rate. 
	 * 	3. Rho is the randomness for exploration.
	 */
	[Range(0.0f, 1.0f)]
	public float alpha = 0.3f;

	[Range(0.0f, 1.0f)]
	public float gamma = 0.75f;

	[Range(0.0f, 1.0f)]
	public float rho   = 0.2f;

	// Rewards and punishments
	public float lapReward = 10.0f;
	public float nonLapPunishment = -0.25f;

	// The starting and lap Destinations
	public Destination lap;

	// Debug
	public bool debug = false;

	public float serializationCooldown = 2.0f;

	public string preloadedQValuesResourceName;
	
	// Start without any training.
	public bool startFromScratch = false;

	/*
	 * ========================================================================================
	 * Private Members
	 * ========================================================================================
	 */

	// An instance of the QLearning algorithm.
	private QLearning<DestinationID, DestinationID> _qLearning;

	// A mapping of IDs an actual Destinations.
	private Dictionary<DestinationID, Destination> _destinations;

	/*
	 * ========================================================================================
	 * Delegates
	 * ========================================================================================
	 */

	/*
	 * The reward is simple: if we get to the lap, we reward. 
	 */
	private float GetReward(DestinationID destinationID){
		if (_destinations[destinationID] == lap) return lapReward;
		else return nonLapPunishment;
	}

	/*
	 * We initialize procedurally by following all destinations.
	 */
	private void InitState(Dictionary<DestinationID, Dictionary<DestinationID, QValue>> QValues){

		var allDestinations = GameObject.FindGameObjectsWithTag("Destination");

		foreach (var gameObject in allDestinations){
			Destination currentDestination = gameObject.GetComponent<Destination>();

			if (currentDestination == null){
				throw new NullReferenceException("There's a non-destination game object with that tag.");
			}

			Dictionary<DestinationID, QValue> qValues = new Dictionary<DestinationID, QValue>();
			foreach (Destination targetDestination in currentDestination.TargetDestinations){
				qValues[targetDestination.ID] = 0.0f;
			}

			QValues[currentDestination.ID] = qValues;
		}
	}

	/*
	 * ========================================================================================
	 * Public Interface
	 * ========================================================================================
	 */
	public Destination Learn(Destination previousState, Destination previousAction, Destination newState){
		DestinationID nextID = _qLearning.Learn(previousState.ID, previousAction.ID, newState.ID);
		return _destinations[nextID];
	}
	
	/*
	 * ========================================================================================
	 * Unity Interface
	 * ========================================================================================
	 */
	/*
	 * On Awake() we load the pre-installed Q-Values for the track.
	 */
	void Awake(){
		// The path to save the QValues of the track.
		Scene currentScene = SceneManager.GetActiveScene();
		_serializationPath = Application.persistentDataPath + "/" + preloadedQValuesResourceName + ".bytes";

		if (!File.Exists(_serializationPath)){
			TextAsset qValuesBinary = Resources.Load<TextAsset>(preloadedQValuesResourceName);

			if (qValuesBinary != null){
				FileStream fs           = new FileStream(_serializationPath, FileMode.Create);
				byte[] bytes = qValuesBinary.bytes;
				fs.Write(bytes, 0, bytes.Length);
				fs.Close();
			}
		}
	}

	void Start(){
		/*
		 * We create a dictionary that maps a Destination's ID to its instance.
		 */
		_destinations       = new Dictionary<DestinationID, Destination>();
		var allDestinations = GameObject.FindGameObjectsWithTag("Destination");

		foreach (var gameObject in allDestinations){
			Destination currentDestination = gameObject.GetComponent<Destination>();

			if (currentDestination == null){
				throw new NullReferenceException("There's a non-destination game object with that tag.");
			}

			_destinations.Add(currentDestination.ID, currentDestination);
		}

		// We initialize our QLearning algorithm.
		_qLearning = new QLearning<DestinationID, DestinationID>(alpha, gamma, rho, GetReward, InitState);

		// We start by loading the stored Q-Values.
		if (File.Exists(_serializationPath) && !startFromScratch){
			_loadQStore = Task.Factory.StartNew( () => { return LoadQValues(_serializationPath); } );
			_serializationEnabled = false;
		}
	}
	
	/*
	 * On Update() we serialize the game object. We provide a cooldown so
	 * that the user has to wait a couple of seconds before doing the action
	 * again (we don't want this executing once per frame). 
	 */
	private float _timer               = 0.0f;
	private bool _serializationEnabled = true;
	private string _serializationPath;
	private Task<Dictionary<DestinationID, Dictionary<DestinationID, QValue>>> _loadQStore = null;

	private void Update(){

		if (Input.GetKeyDown(UnityEngine.KeyCode.O) && _serializationEnabled){
			_serializationEnabled = false;
			Task.Run(() => SaveQValues(_serializationPath, _qLearning.QValues));
		}

		if (Input.GetKeyDown(KeyCode.K) && _serializationEnabled){
			_serializationEnabled = false;
			_loadQStore = Task.Factory.StartNew( () => { return LoadQValues(_serializationPath); } );
		}

		if (_loadQStore != null && _loadQStore.IsCompleted){
			_qLearning.QValues = _loadQStore.Result;
			_loadQStore = null;
		}

		if (_serializationEnabled == false){
			_timer += Time.deltaTime;
			if (_timer > serializationCooldown){
				_serializationEnabled = true;
				_timer = 0.0f;
			}
		}

		/*
		 * We do this so that we can modify the algorithm's 
		 * behavior in real time.
		 */
		_qLearning.Alpha        = alpha;
		_qLearning.Gamma        = gamma;
		_qLearning.Rho          = rho;
		_qLearning.DebugEnabled = debug;
	}

	/*
	 * ========================================================================================
	 * Serializing Methods
	 * ========================================================================================
	 */
	private void SaveQValues(string serializationPath, Dictionary<DestinationID, Dictionary<DestinationID, QValue>> qValueStore){
		FileStream fs             = new FileStream(serializationPath, FileMode.Create);
		BinaryFormatter formatter = new BinaryFormatter();
		formatter.Serialize(fs, qValueStore);
		fs.Close();
	}

	private Dictionary<DestinationID, Dictionary<DestinationID, QValue>> LoadQValues(string serializationPath){
		Dictionary<DestinationID, Dictionary<DestinationID, QValue>> result = null;
		FileStream fs                                                       = new FileStream(serializationPath, FileMode.Open);
		BinaryFormatter formatter                                           = new BinaryFormatter();
		result = (Dictionary<DestinationID, Dictionary<DestinationID, QValue>>) formatter.Deserialize(fs);
		return result;
	}
}
