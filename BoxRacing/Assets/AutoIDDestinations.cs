﻿using System;
using UnityEngine;

[ExecuteInEditMode]
public class AutoIDDestinations : MonoBehaviour {

	void OnEnable(){
		var allDestinations = GameObject.FindGameObjectsWithTag("Destination");

		for (int id = 0; id < allDestinations.Length; id++){

			Destination current = allDestinations[id].GetComponent<Destination>();
			if (current == null){
				throw new Exception("There's a non-Destination gameObject with the Destination tag on it.");
			}

			current.ID = id;
		}
	}
}
