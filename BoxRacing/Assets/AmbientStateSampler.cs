using UnityEngine;

/*
 * This is one of the most important scripts in the game. It's role 
 * is to sample, on every FixedUpdate(), the current state of the Cart
 * and forge a CartState data structure saved in _stateBuffer. Review 
 * CartState.cs and Cart.cs before proceeding to make sense of this script. 
 * The CartState computed by AmbientStateSampler is used by
 * QLearnedManipulator to interface with MovementQLearning and get the next
 * best action as calculated by the Q-Learning algorithm. 
 */

[RequireComponent(typeof(Rigidbody), typeof(Cart))]
public class AmbientStateSampler : MonoBehaviour {

	/*
	 * ========================================================================================
	 * Unity Interface Variables
	 * ========================================================================================
	 */

	/*
	 * Whether Cart is Clever Cart or Slow Cart. 
	 */
	public bool isClever = true;

	// Whether to output debug rays. 
	public bool debug = false;

	/*
	 * Number of rays to launch to sample which other Cart or obstacle is nearby.
	 */
       public int numberOfSamplingRays = 32;	

       /*
	* Number of rays to sample the Target Destination. 
	*/
       public int numberOfTargetRays = 4;

       /*
	* The radius of the sphere cast of determine the optimal CurrentRoadDirection.
	*/
       public float sphereRadius = 1.0f;

	/*
	 * How much of the vector pointing between intersections
	 * is hardset onto the CurrentRoadDirection calculation.
	 */
	public float proportionOfIntersection = 0.75f;

	/*
	 * The a function which expresses the distance of the sampling rays. We do this 
	 * because we don't want the rays shooting backwards to be the same size as those
	 * shooting forward. 
	 */
	public AnimationCurve minimumNearbyDistanceCurve;

	/*
	 * The minimum value before a collision is considered 
	 * imminent.
	 */
	public float imminentCollisionValue = 2.0f;

	/*
	 * The transforms that guide the current destination
	 * for the Cart. 
	 */
	public Destination originDestination;
	public Destination targetDestination;

	

	/*
	 * ========================================================================================
	 * Public Properties
	 * ========================================================================================
	 */

	/*
	 * How fast are we going relative to our maximum attained speed. This maximum 
	 * value for this field is defined in Configuration.cs.
	 */
	public sbyte DegreeOfSpeed { get; private set; }

	/*
	 * ========================================================================================
	 * Private Variables
	 * ========================================================================================
	 */

	// Holds the sampling-ray's directions in local coordinates. 
	private Vector3[] _localDirections;

	private Rigidbody _rigidbody       = null;
	private Cart      _cart            = null;

	// Where we keep the sampled state. 
	private CartState _stateBuffer;

	// To calculate CartState.DegreeOfSpeed we need to know
	// what's the maximum attained speed by the cart. We start at 
	// 1.0f, but surely greater speeds will be attained, and we must sample them.
	private float _maxSpeed            = 1.0f;
	
	/*
	 * ========================================================================================
	 * Private Interface
	 * ========================================================================================
	 */
	/*
	 * This is a crucial method that quantizes a vector into N distinct directions. Here's 
	 * how it works. 
	 *
	 * First, note that 2 * PI / N is the amount of degrees covered by each quantized direction. 
	 * For example, if we have a vector that whose Atan2 is 40-degrees, and if N is 16, then 
	 * the expanse of each direction is 22.5-degrees, so that our vector (which was 40-degrees), 
	 * lies between (22.5, 45) degrees, meaning that it's quantized direction is 1. If we have
	 * a vector whose Atan2 is 15-degrees, it lies between (337.5, 22.5) degrees, meaning it has
	 * a direction of 0.
	 *
	 * For the zeroth direction, the vector's theta must be in the range (2 * PI - PI / N, PI / N). 
	 * So, for example, if N = 16, then the zeroth direction lies in the range of (337.5, 22.5) degrees 
	 * (modulo 360, of course). 
	 *
	 * For directions ( 0 < i < N ) we have that theta must be in the range 
	 * ( PI / N + (i - 1) * 2 * PI / N, PI / N + i * 2 * PI / N). If
	 * we solve for i in the second part of the range we have:
	 *
	 * theta < PI / N + i * 2 * PI / N. 
	 *
	 * Solving for i we have:
	 *
	 * i > (theta * N / PI - 1) / 2
	 *
	 * We apply the ceiling function and we get the result we want. 
	 *
	 */
	private sbyte QuantizeVector(Vector3 direction){
		float theta = Mathf.Atan2(direction.x, direction.z);
		
		// This is a trick to have theta between [0, 360] degrees. 
		if (theta < 0.0f) theta += Mathf.PI * 2.0f;

		float PIbyN  = Mathf.PI / Configuration.N;
		float NbyPI = 1.0f / PIbyN;

		if (theta >= 2.0f * Mathf.PI - PIbyN || theta < PIbyN)
			return 0;

		return (sbyte) Mathf.Ceil( (NbyPI * theta - 1.0f) / 2); 
	}

	/*
	 * ========================================================================================
	 * Public Interface
	 * ========================================================================================
	 */

	/*
	 * The main reason this MonoBehavior exists: so that we can get the current Cart
	 * state on each frame. But I would advice to read FixedUpdate() below first before trying
	 * this one.
	 */
	public (Vector3, CartState) GetState(){

		// Calculate DegreeOfSpeed ---------------------------------------------------------
		Vector3 planarVelocity     = new Vector3(_rigidbody.velocity.x, 0.0f, _rigidbody.velocity.z);
		float currentSpeed         = planarVelocity.magnitude;
		_stateBuffer.DegreeOfSpeed = (sbyte) ((float) Configuration.DegreesOfSpeed * (currentSpeed / _maxSpeed));

		Vector3 CurrentRoadDirection;
		if (isClever){
			CurrentRoadDirection = CalculateCleverCartCRD();
		} else {
			CurrentRoadDirection = CalculateSlowCartCRD();
		}

		/*
		 * Calcultate FacingDirection ------------------------------------------------------
		 * Here we calculate the FacingDirection. We create a rotation matrix given the CRD's
		 * orientation, then rotate our planarVelocity vector and get its theta. With that, 
		 * we discretize theta between Configuration.N values. 
		 */
		float CRDtheta      = Mathf.Atan2(CurrentRoadDirection.x, CurrentRoadDirection.z);
		Quaternion rotation = Quaternion.Euler(0, -CRDtheta * Mathf.Rad2Deg, 0);

		// This matrix is a linear transformation that will rotate any vector in direction
		// oposite of -CRDTheta, giving us a vector in a local space ready for quantization.
		Matrix4x4 m         = Matrix4x4.Rotate(rotation);

		Vector3 facingDirection          = planarVelocity.normalized;
		Vector3 localizedFacingDirection = m.MultiplyVector(facingDirection);
		_stateBuffer.FacingDirection     = QuantizeVector(localizedFacingDirection);

		/*
		 * We draw a debug ray for our current velocity.
		 */
		if (debug){
			Debug.DrawRay(transform.position, facingDirection * 7.0f, Color.magenta, 0.2f);
		}

		/*
		 * Launch all Rays -----------------------------------------------------------------
		 * Here we launch all the rays to detect any nearby objects.
		 */

		float minimumDistance                    = Mathf.Infinity;
		Rigidbody nearestObjectRB                = null;
		AmbientStateSampler nearestObjectSampler = null;
		Vector3 nearestObjectDirection           = Vector3.zero;
		GameObject nearestObject                 = null;
		Vector3 rayNacentPosition                = transform.position;
		int layerMask 				 = 1 << LayerMask.NameToLayer("Obstacles");

    		for (int i = 0; i < _localDirections.Length; i++){
			Vector3 rayDirection        = transform.TransformDirection(_localDirections[i]);
			float minimumNearbyDistance = minimumNearbyDistanceCurve.Evaluate((float)i/_localDirections.Length);
			RaycastHit[] hits           = Physics.RaycastAll(
								rayNacentPosition, 
								rayDirection, 
								minimumNearbyDistance, 
								layerMask, 
								QueryTriggerInteraction.Ignore
							);

			foreach (var hit in hits){
				if (hit.distance < minimumDistance){
					minimumDistance        = hit.distance;
					nearestObjectDirection = rayDirection;
					nearestObjectRB        = hit.rigidbody;
					nearestObjectSampler   = hit.transform.GetComponent<AmbientStateSampler>();
					nearestObject          = hit.transform.gameObject;
				}
			}

			if (debug){
				if (hits.Length > 0)
					Debug.DrawRay(rayNacentPosition, rayDirection * minimumNearbyDistance, Color.red, 0.2f);
				else 
					Debug.DrawRay(rayNacentPosition, rayDirection * minimumNearbyDistance, Color.green, 0.2f);
		    	}
	        }

		/*
		 * Set ImminentCollision and NearestObjectDirection --------------------------------
		 */
		if (minimumDistance != Mathf.Infinity){
			Vector3 localizedRayHitDirection    = m.MultiplyVector(nearestObjectDirection.normalized);
			_stateBuffer.NearestObjectDirection = QuantizeVector(localizedRayHitDirection);

			if (nearestObjectRB != null){
				Vector3 localizedObjectVelocity    = m.MultiplyVector(nearestObjectRB.velocity.normalized);
				_stateBuffer.NearestObjectVelocity = QuantizeVector(localizedObjectVelocity);
			}

			if (nearestObjectSampler != null){
				_stateBuffer.NearestObjectDegreeOfSpeed = nearestObjectSampler.DegreeOfSpeed;
			} else {
				// Case for player
				if (nearestObject.tag == "Player"){
					Cart cart = nearestObject.GetComponent<Cart>();
					_stateBuffer.NearestObjectDegreeOfSpeed = (sbyte) ((float) Configuration.DegreesOfSpeed * (nearestObjectRB.velocity.magnitude / cart.MaxSpeedReached));
					
				}
			}

			if (minimumDistance < imminentCollisionValue){
				_stateBuffer.ImminentCollision = true;
			}
		}

		CartState toReturn = _stateBuffer;

		toReturn.IsRestarting = _cart.IsRestarting;

		/*
		 * Re-initialize the buffer.
		 */
		_stateBuffer                            = new CartState();
		_stateBuffer.FacingDirection            = 0;
		_stateBuffer.NearestObjectDirection     = -1;
		_stateBuffer.NearestObjectVelocity      = -1;
		_stateBuffer.NearestObjectDegreeOfSpeed = -1;
		_stateBuffer.ImminentCollision          = false;
		_stateBuffer.IsRestarting                 = false;

		return (CurrentRoadDirection, toReturn);
	}

	/*
	 * ----------------------------------------------------------------------------------------
	 *  Private Interface
	 * ----------------------------------------------------------------------------------------
	 */


	private Vector3 _accumulatedCRD = Vector3.zero;
	private float _alpha = 0.95f;
	private Vector3 CalculateSlowCartCRD(){
		Collider targetCollider = targetDestination.GetComponent<Collider>();
		Vector3 closestPoint    = targetCollider.ClosestPoint(transform.position);

		Vector3 V = Vector3.Normalize(closestPoint - transform.position);

		_accumulatedCRD = _alpha * _accumulatedCRD + (1 - _alpha) * V;

		return _accumulatedCRD;

	}

	private Vector3 CalculateCleverCartCRD(){

		/*
		 * Calculate CurrentRoadDirection --------------------------------------------------
		 * Here we calculate the CurrentRoadDirection. We do this by interpolating two distinct
		 * vectors. First we have the intersectionDisplacement vector, which is the displacement
		 * between the origin and destination transforms (i.e., it holds the displacement that
		 * the cart is going to have to traverse for this intersection). cartDisplacement is
		 * the second vector, and it is the displacement the vector that provides the "least
		 * resistance", from the Carts current position, on the way to the target Destination.
		 */ 

		Vector3 min             = targetDestination.Min.position;
		Vector3 max             = targetDestination.Max.position;
		float average_y         = (min.y + max.y) / 2;

		Vector3 start           = new Vector3(min.x, average_y, min.z);
		Vector3 finish          = new Vector3(max.x, average_y, max.z);

		Vector3 displacement    = finish - start;

		int layerMask           = 1 << LayerMask.NameToLayer("Obstacles");

		Vector3 leastResistance      = Vector3.zero;
		int minCount                 = System.Int32.MaxValue;

		/*
		 * Here we launch a Sphere to different target points on the targetDestination. 
		 * The Sphere that touches the least amount of obstacles provides the path 
		 * of least resistance. 
		 */
		for (int i = 1; i < numberOfTargetRays + 1; i++){
			Vector3 target    = displacement * ((float) i / ((float) numberOfTargetRays + 1));
			float distance    = Vector3.Magnitude(target - transform.position) + 1.0f;
			Vector3 direction = (start + target) - transform.position;

			RaycastHit[] hits = Physics.SphereCastAll( 
							 	transform.position,
								sphereRadius,
								direction.normalized,
								distance, 
								layerMask,
								QueryTriggerInteraction.Collide
							);

			if (debug){
				Debug.DrawRay(transform.position, direction, Color.yellow, 0.2f);
			}

			if (hits.Length <= minCount){
					minCount        = hits.Length;
					leastResistance = direction;
			}
		}


		// Calculate intersection and cart displacement.
		Vector3 intersectionDisplacement = targetDestination.transform.position - originDestination.transform.position;
		Vector3 cartDisplacement         = transform.position - originDestination.transform.position;

		// This is the degree of how close is the cart to the Destination of this intersection. 
		float alpha                      = Vector3.Dot(cartDisplacement, intersectionDisplacement) / Mathf.Pow(intersectionDisplacement.magnitude, 2);

		// Getting Collider information
		Collider targetCollider = targetDestination.GetComponent<Collider>();
		Vector3 closestPoint    = targetCollider.ClosestPoint(transform.position);
	
		// We calculate the direction vectors
		Vector3 intersectionDirection = Vector3.Normalize(closestPoint - transform.position);
		Vector3 destinationDirection  = Vector3.Normalize(leastResistance);
		if (debug) Debug.DrawRay(closestPoint, transform.position - closestPoint, Color.white, 0.2f);

		/*
		 * The closer we're to our destination, the more the destinationDirection vector
		 * counts. To avoid the effect of having the carts turn abrumptly towards the
		 * destinationDirection, we provide a proportionOfIntersection which limits 
		 * this interpolation. 
		 */
		alpha                        = proportionOfIntersection * Mathf.Pow(alpha, 12.0f);
		Vector3 CurrentRoadDirection = (1 - alpha) * destinationDirection + alpha * intersectionDirection;
		CurrentRoadDirection         = new Vector3(CurrentRoadDirection.x, 0.0f, CurrentRoadDirection.z);
		CurrentRoadDirection.Normalize();
		
		const float CRDRayMagnitude = 5.0f;
		if (debug) Debug.DrawRay(transform.position, CurrentRoadDirection * CRDRayMagnitude, Color.black);

		return CurrentRoadDirection;
	}

	/*
	 * ========================================================================================
	 * Unity Interface
	 * ========================================================================================
	 */

	/*
	 * We initialize our data structures. 
	 * NearestObjectDirection of -1 means there's no object currently nearby.
	 */
	void Awake() {
		_rigidbody = GetComponent<Rigidbody>();
		_cart      = GetComponent<Cart>();

		_stateBuffer                            = new CartState();
		_stateBuffer.FacingDirection            = 0;
		_stateBuffer.NearestObjectDirection     = -1;
		_stateBuffer.NearestObjectVelocity      = -1;
		_stateBuffer.NearestObjectDegreeOfSpeed = -1;
		_stateBuffer.ImminentCollision          = false;
		_stateBuffer.IsRestarting               = false;
	}

	/*
	 * We initialize the ray directions in local space. We start at 90-degrees
	 * because we want ray 0 to be the front ray, pointing at (0, 0, 1). 
	 */
	void Start() {
    	    float theta      = Mathf.PI / 2;
	    float deltaTheta = 2.0f * Mathf.PI / numberOfSamplingRays;
	    _localDirections = new Vector3[numberOfSamplingRays];

	    for (int i = 0; i < _localDirections.Length; i++){
		    _localDirections[i] = new Vector3(Mathf.Cos(theta), 0, Mathf.Sin(theta));
		    theta -= deltaTheta;
	    }
    	}

	/*
	 * Update() has several responsibilities:
	* 	1. Sample the _maxSpeed.
	* 	2. Calculate the CurrentRoadDirection
	 */
    	void FixedUpdate() {
		/*
		 * We sample the max velocity. ----------------------------------------------------
		 */
		Vector3 planarVelocity = new Vector3(_rigidbody.velocity.x, 0.0f, _rigidbody.velocity.z);
		float currentSpeed     = planarVelocity.magnitude;
		if (currentSpeed > _maxSpeed)
			_maxSpeed = currentSpeed;

    	}
}
