<Q                             VLB_COLOR_GRADIENT_MATRIX_LOW      VLB_DEPTH_BLEND    VLB_NOISE_3D   VLB_OCCLUSION_DEPTH_TEXTURE !E  #ifdef VERTEX
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
vec4 ImmCB_0_0_0[4];
uniform 	vec4 _Time;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ProjectionParams;
uniform 	vec4 unity_OrthoParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 hlslcc_mtx4x4_ColorGradientMatrix[4];
uniform 	float _AlphaInside;
uniform 	float _AlphaOutside;
uniform 	vec2 _ConeRadius;
uniform 	float _AttenuationLerpLinearQuad;
uniform 	float _DistanceFadeStart;
uniform 	float _DistanceFadeEnd;
uniform 	float _FadeOutFactor;
uniform 	float _GlareFrontal;
uniform 	float _DrawCap;
uniform 	vec4 _CameraParams;
uniform 	vec4 _NoiseLocal;
uniform 	vec4 _NoiseParam;
uniform 	vec4 _VLB_NoiseGlobal;
in  vec4 in_POSITION0;
in  vec4 in_TEXCOORD0;
out vec3 vs_TEXCOORD0;
out vec4 vs_TEXCOORD1;
out vec4 vs_TEXCOORD2;
out vec4 vs_TEXCOORD3;
out vec4 vs_TEXCOORD4;
out vec4 vs_TEXCOORD5;
out vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
bool u_xlatb1;
vec4 u_xlat2;
vec4 u_xlat3;
vec4 u_xlat4;
vec4 u_xlat5;
vec2 u_xlat6;
vec3 u_xlat8;
int u_xlati8;
uint u_xlatu8;
bool u_xlatb8;
vec2 u_xlat9;
vec2 u_xlat15;
uint u_xlatu15;
bool u_xlatb15;
float u_xlat21;
float u_xlat22;
int u_xlati22;
bool u_xlatb22;
float u_xlat23;
void main()
{
	ImmCB_0_0_0[0] = vec4(1.0, 0.0, 0.0, 0.0);
	ImmCB_0_0_0[1] = vec4(0.0, 1.0, 0.0, 0.0);
	ImmCB_0_0_0[2] = vec4(0.0, 0.0, 1.0, 0.0);
	ImmCB_0_0_0[3] = vec4(0.0, 0.0, 0.0, 1.0);
    u_xlat0.xy = max(_ConeRadius.yy, _ConeRadius.xx);
    u_xlat1.xy = _ConeRadius.xy / u_xlat0.yy;
    u_xlat21 = (-u_xlat1.x) + u_xlat1.y;
    u_xlat2.z = in_POSITION0.z * in_POSITION0.z;
    u_xlat21 = u_xlat2.z * u_xlat21 + u_xlat1.x;
    u_xlat2.xy = vec2(u_xlat21) * in_POSITION0.xy;
    u_xlat1 = u_xlat2.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat2.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat2.zzzz + u_xlat1;
    u_xlat3 = u_xlat1 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[3] * in_POSITION0.wwww + u_xlat1;
    u_xlat4 = u_xlat3.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat4 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat3.xxxx + u_xlat4;
    u_xlat4 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat3.zzzz + u_xlat4;
    u_xlat3 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat3.wwww + u_xlat4;
    gl_Position = u_xlat3;
    u_xlat0.z = _DistanceFadeEnd;
    u_xlat4.xyz = u_xlat0.xyz * u_xlat2.xyz;
    vs_TEXCOORD0.xyz = u_xlat4.xyz;
    vs_TEXCOORD1 = u_xlat1;
    u_xlat4.xyw = u_xlat1.yyy * hlslcc_mtx4x4unity_MatrixV[1].xyz;
    u_xlat4.xyw = hlslcc_mtx4x4unity_MatrixV[0].xyz * u_xlat1.xxx + u_xlat4.xyw;
    u_xlat4.xyw = hlslcc_mtx4x4unity_MatrixV[2].xyz * u_xlat1.zzz + u_xlat4.xyw;
    u_xlat4.xyw = u_xlat4.xyw + hlslcc_mtx4x4unity_MatrixV[3].xyz;
    vs_TEXCOORD2.xyz = u_xlat4.xyw;
    vs_TEXCOORD6.z = (-u_xlat4.w);
    u_xlat21 = _GlareFrontal * _DistanceFadeEnd + 0.00100000005;
    u_xlat21 = float(1.0) / u_xlat21;
    u_xlat22 = max(abs(u_xlat4.z), 0.00100000005);
    u_xlat21 = u_xlat21 * u_xlat22;
    u_xlat21 = clamp(u_xlat21, 0.0, 1.0);
    u_xlat22 = u_xlat21 * -2.0 + 3.0;
    u_xlat21 = u_xlat21 * u_xlat21;
    u_xlat21 = (-u_xlat22) * u_xlat21 + 1.0;
    u_xlat21 = in_TEXCOORD0.y * (-u_xlat21) + u_xlat21;
    u_xlat22 = _CameraParams.w;
    u_xlat22 = clamp(u_xlat22, 0.0, 1.0);
    u_xlat23 = u_xlat21 * u_xlat22;
    u_xlat21 = (-u_xlat22) * u_xlat21 + 1.0;
    vs_TEXCOORD2.w = in_TEXCOORD0.x * u_xlat21 + u_xlat23;
    u_xlat4.xyw = _WorldSpaceCameraPos.yyy * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat4.xyw = hlslcc_mtx4x4unity_WorldToObject[0].xyz * _WorldSpaceCameraPos.xxx + u_xlat4.xyw;
    u_xlat4.xyw = hlslcc_mtx4x4unity_WorldToObject[2].xyz * _WorldSpaceCameraPos.zzz + u_xlat4.xyw;
    u_xlat4.xyw = u_xlat4.xyw + hlslcc_mtx4x4unity_WorldToObject[3].xyz;
    u_xlat4.xyw = u_xlat0.xyz * u_xlat4.xyw;
    vs_TEXCOORD3.xyz = u_xlat4.xyw;
    u_xlat4.xyw = u_xlat2.xyz * u_xlat0.xyz + (-u_xlat4.xyw);
    u_xlat0.xyz = u_xlat2.xyz * u_xlat0.xyz + (-u_xlat1.xyz);
    u_xlat0.xyz = _NoiseParam.www * u_xlat0.xyz + u_xlat1.xyz;
    vs_TEXCOORD3.w = in_TEXCOORD0.y;
    u_xlat21 = abs(u_xlat4.z) / _DistanceFadeEnd;
    u_xlat21 = clamp(u_xlat21, 0.0, 1.0);
    u_xlat1.x = abs(u_xlat4.z) + (-_DistanceFadeStart);
    u_xlat8.x = u_xlat21 * 15.0;
    u_xlatu15 = uint(u_xlat8.x);
    u_xlat8.x = trunc(u_xlat8.x);
    u_xlat21 = u_xlat21 * 15.0 + (-u_xlat8.x);
    u_xlatu8 = min(u_xlatu15, 14u);
    u_xlatu15 = u_xlatu8 >> 2u;
    u_xlati22 = int(u_xlatu8 & 3u);
    u_xlatu8 = u_xlatu8 + 1u;
    u_xlat2.x = dot(hlslcc_mtx4x4_ColorGradientMatrix[0], ImmCB_0_0_0[u_xlati22]);
    u_xlat2.y = dot(hlslcc_mtx4x4_ColorGradientMatrix[1], ImmCB_0_0_0[u_xlati22]);
    u_xlat2.z = dot(hlslcc_mtx4x4_ColorGradientMatrix[2], ImmCB_0_0_0[u_xlati22]);
    u_xlat2.w = dot(hlslcc_mtx4x4_ColorGradientMatrix[3], ImmCB_0_0_0[u_xlati22]);
    u_xlat15.x = dot(u_xlat2, ImmCB_0_0_0[int(u_xlatu15)]);
    u_xlat2.xy = u_xlat15.xx * vec2(8.0, 0.125);
    u_xlatb22 = u_xlat2.x>=(-u_xlat2.x);
    u_xlat2.x = floor(u_xlat2.y);
    u_xlat9.xy = (bool(u_xlatb22)) ? vec2(8.0, 0.125) : vec2(-8.0, -0.125);
    u_xlat15.x = u_xlat15.x * u_xlat9.y;
    u_xlat15.x = fract(u_xlat15.x);
    u_xlat5.w = u_xlat15.x * u_xlat9.x;
    u_xlat15.xy = u_xlat2.xx * vec2(8.0, 0.125);
    u_xlatb15 = u_xlat15.x>=(-u_xlat15.x);
    u_xlat22 = floor(u_xlat15.y);
    u_xlat9.xy = (bool(u_xlatb15)) ? vec2(8.0, 0.125) : vec2(-8.0, -0.125);
    u_xlat15.x = u_xlat9.y * u_xlat2.x;
    u_xlat15.x = fract(u_xlat15.x);
    u_xlat5.z = u_xlat15.x * u_xlat9.x;
    u_xlat2.xy = vec2(u_xlat22) * vec2(8.0, 0.125);
    u_xlatb15 = u_xlat2.x>=(-u_xlat2.x);
    u_xlat5.x = floor(u_xlat2.y);
    u_xlat2.xy = (bool(u_xlatb15)) ? vec2(8.0, 0.125) : vec2(-8.0, -0.125);
    u_xlat15.x = u_xlat22 * u_xlat2.y;
    u_xlat15.x = fract(u_xlat15.x);
    u_xlat5.y = u_xlat15.x * u_xlat2.x;
    u_xlat2 = u_xlat5 * vec4(0.142857149, 0.142857149, 0.142857149, 0.142857149);
    u_xlatu15 = u_xlatu8 >> 2u;
    u_xlati8 = int(u_xlatu8 & 3u);
    u_xlat5.x = dot(hlslcc_mtx4x4_ColorGradientMatrix[0], ImmCB_0_0_0[u_xlati8]);
    u_xlat5.y = dot(hlslcc_mtx4x4_ColorGradientMatrix[1], ImmCB_0_0_0[u_xlati8]);
    u_xlat5.z = dot(hlslcc_mtx4x4_ColorGradientMatrix[2], ImmCB_0_0_0[u_xlati8]);
    u_xlat5.w = dot(hlslcc_mtx4x4_ColorGradientMatrix[3], ImmCB_0_0_0[u_xlati8]);
    u_xlat8.x = dot(u_xlat5, ImmCB_0_0_0[int(u_xlatu15)]);
    u_xlat15.xy = u_xlat8.xx * vec2(8.0, 0.125);
    u_xlatb15 = u_xlat15.x>=(-u_xlat15.x);
    u_xlat22 = floor(u_xlat15.y);
    u_xlat5.xy = (bool(u_xlatb15)) ? vec2(8.0, 0.125) : vec2(-8.0, -0.125);
    u_xlat8.x = u_xlat8.x * u_xlat5.y;
    u_xlat8.x = fract(u_xlat8.x);
    u_xlat5.w = u_xlat8.x * u_xlat5.x;
    u_xlat8.xy = vec2(u_xlat22) * vec2(8.0, 0.125);
    u_xlatb8 = u_xlat8.x>=(-u_xlat8.x);
    u_xlat15.x = floor(u_xlat8.y);
    u_xlat6.xy = (bool(u_xlatb8)) ? vec2(8.0, 0.125) : vec2(-8.0, -0.125);
    u_xlat8.x = u_xlat22 * u_xlat6.y;
    u_xlat8.x = fract(u_xlat8.x);
    u_xlat5.z = u_xlat8.x * u_xlat6.x;
    u_xlat8.xz = u_xlat15.xx * vec2(8.0, 0.125);
    u_xlatb8 = u_xlat8.x>=(-u_xlat8.x);
    u_xlat5.x = floor(u_xlat8.z);
    u_xlat8.xz = (bool(u_xlatb8)) ? vec2(8.0, 0.125) : vec2(-8.0, -0.125);
    u_xlat15.x = u_xlat8.z * u_xlat15.x;
    u_xlat15.x = fract(u_xlat15.x);
    u_xlat5.y = u_xlat15.x * u_xlat8.x;
    u_xlat5 = u_xlat5 * vec4(0.142857149, 0.142857149, 0.142857149, 0.142857149) + (-u_xlat2);
    u_xlat2 = vec4(u_xlat21) * u_xlat5 + u_xlat2;
    u_xlat21 = (-_AlphaInside) + _AlphaOutside;
    u_xlat21 = in_TEXCOORD0.y * u_xlat21 + _AlphaInside;
    vs_TEXCOORD4.w = u_xlat21 * u_xlat2.w;
    vs_TEXCOORD4.xyz = u_xlat2.xyz;
    u_xlat21 = dot(u_xlat4.xyw, u_xlat4.xyw);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat8.x = u_xlat21 * u_xlat4.w;
    u_xlat21 = (-u_xlat4.w) * u_xlat21 + _CameraParams.z;
    u_xlat21 = unity_OrthoParams.w * u_xlat21 + u_xlat8.x;
    u_xlat21 = u_xlat21 + -1.0;
    u_xlat21 = u_xlat21 * -0.5;
    u_xlat21 = clamp(u_xlat21, 0.0, 1.0);
    u_xlat8.x = u_xlat21 * -2.0 + 3.0;
    u_xlat21 = u_xlat21 * u_xlat21;
    u_xlat21 = u_xlat21 * u_xlat8.x;
    u_xlat21 = min(u_xlat21, 1.0);
    u_xlat8.x = u_xlat21 * -2.0 + 1.0;
    u_xlat21 = in_TEXCOORD0.y * u_xlat8.x + u_xlat21;
    u_xlat8.x = (-_DistanceFadeStart) + _DistanceFadeEnd;
    u_xlat1.x = u_xlat1.x / u_xlat8.x;
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
    u_xlat8.x = u_xlat1.x + -1.0;
    u_xlat8.x = u_xlat8.x * -5.00000048;
    u_xlat8.x = min(u_xlat8.x, 1.0);
    u_xlat15.x = u_xlat8.x * -2.0 + 3.0;
    u_xlat8.x = u_xlat8.x * u_xlat8.x;
    u_xlat8.x = u_xlat8.x * u_xlat15.x;
    u_xlat8.x = min(u_xlat8.x, 1.0);
    u_xlat15.x = u_xlat1.x * u_xlat1.x;
    u_xlat1.x = (-u_xlat1.x) + 1.0;
    u_xlat15.x = u_xlat15.x * 25.0 + 1.0;
    u_xlat15.x = float(1.0) / u_xlat15.x;
    u_xlat22 = u_xlat1.x * -2.0 + 3.0;
    u_xlat1.x = u_xlat1.x * u_xlat1.x;
    u_xlat1.x = u_xlat1.x * u_xlat22;
    u_xlat8.x = u_xlat15.x * u_xlat8.x + (-u_xlat1.x);
    u_xlat1.x = _AttenuationLerpLinearQuad * u_xlat8.x + u_xlat1.x;
    u_xlat8.x = (-in_TEXCOORD0.y) * in_TEXCOORD0.x + 1.0;
    u_xlat1.x = u_xlat1.x * u_xlat8.x;
    u_xlat1.x = u_xlat1.x * _FadeOutFactor;
    u_xlat21 = u_xlat21 * u_xlat1.x;
    u_xlatb1 = _DrawCap>=in_TEXCOORD0.x;
    u_xlat1.x = u_xlatb1 ? 1.0 : float(0.0);
    vs_TEXCOORD5.w = u_xlat21 * u_xlat1.x;
    u_xlat1 = (-_NoiseLocal) + _VLB_NoiseGlobal;
    u_xlat1 = _NoiseParam.yyyz * u_xlat1 + _NoiseLocal;
    u_xlat1.xyz = u_xlat1.xyz * _Time.yyy;
    vs_TEXCOORD5.xyz = u_xlat0.xyz * u_xlat1.www + u_xlat1.xyz;
    u_xlat0.x = u_xlat3.y * _ProjectionParams.x;
    u_xlat0.w = u_xlat0.x * 0.5;
    u_xlat0.xz = u_xlat3.xw * vec2(0.5, 0.5);
    vs_TEXCOORD6.w = u_xlat3.w;
    vs_TEXCOORD6.xy = u_xlat0.zz + u_xlat0.xw;
    return;
}

#endif
#ifdef FRAGMENT
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ProjectionParams;
uniform 	vec4 _ZBufferParams;
uniform 	vec4 unity_OrthoParams;
uniform 	vec4 unity_CameraWorldClipPlanes[6];
uniform 	vec2 _ConeSlopeCosSin;
uniform 	float _ConeApexOffsetZ;
uniform 	float _DistanceFadeEnd;
uniform 	float _DistanceCamClipping;
uniform 	float _FresnelPow;
uniform 	float _GlareFrontal;
uniform 	float _GlareBehind;
uniform 	vec4 _CameraParams;
uniform 	float _DynamicOcclusionDepthProps;
uniform 	float _DepthBlendDistance;
uniform 	vec4 _NoiseParam;
UNITY_LOCATION(0) uniform  sampler2D _DynamicOcclusionDepthTexture;
UNITY_LOCATION(1) uniform  sampler3D _VLB_NoiseTex3D;
UNITY_LOCATION(2) uniform  sampler2D _CameraDepthTexture;
in  vec3 vs_TEXCOORD0;
in  vec4 vs_TEXCOORD2;
in  vec4 vs_TEXCOORD3;
in  vec4 vs_TEXCOORD4;
in  vec4 vs_TEXCOORD5;
in  vec4 vs_TEXCOORD6;
layout(location = 0) out vec4 SV_Target0;
vec3 u_xlat0;
bool u_xlatb0;
vec4 u_xlat1;
vec3 u_xlat2;
vec4 u_xlat3;
float u_xlat4;
bool u_xlatb4;
float u_xlat5;
vec2 u_xlat8;
vec2 u_xlat10;
float u_xlat12;
bool u_xlatb12;
void main()
{
    u_xlat0.x = 0.0;
    u_xlat0.z = _ConeApexOffsetZ;
    u_xlat0.xyz = u_xlat0.xxz + vs_TEXCOORD0.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat1.xyz = vs_TEXCOORD0.xyz + (-vs_TEXCOORD3.xyz);
    u_xlat12 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat2.xyz = vec3(u_xlat12) * u_xlat1.xyz;
    u_xlat1.xyz = (-u_xlat1.xyz) * vec3(u_xlat12) + _CameraParams.xyz;
    u_xlat1.xyz = unity_OrthoParams.www * u_xlat1.xyz + u_xlat2.xyz;
    u_xlat12 = dot((-u_xlat1.xyz), u_xlat0.xyz);
    u_xlat0.xyz = (-vec3(u_xlat12)) * u_xlat0.xyz + (-u_xlat1.xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat12 = dot(vs_TEXCOORD0.xy, vs_TEXCOORD0.xy);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat2.xy = vec2(u_xlat12) * vs_TEXCOORD0.xy;
    u_xlat10.xy = u_xlat2.xy * vec2(_ConeSlopeCosSin.x, _ConeSlopeCosSin.x);
    u_xlat2.xy = u_xlat2.xy * vec2(0.5, 0.5) + vec2(0.5, 0.5);
    u_xlat3 = texture(_DynamicOcclusionDepthTexture, u_xlat2.xy);
    u_xlat12 = vs_TEXCOORD3.w * 2.0 + -1.0;
    u_xlat2.xy = vec2(u_xlat12) * u_xlat10.xy;
    u_xlat2.z = u_xlat12 * (-_ConeSlopeCosSin.xxxy.w);
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat0.xyz);
    u_xlat4 = dot(u_xlat2.xyz, (-u_xlat1.xyz));
    u_xlat4 = (-u_xlat0.x) + u_xlat4;
    u_xlat0.x = abs(u_xlat1.z) * u_xlat4 + u_xlat0.x;
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
    u_xlat4 = u_xlat0.x * -2.0 + 3.0;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat4;
    u_xlat4 = log2(u_xlat0.x);
    u_xlatb0 = (-u_xlat0.x)>=0.0;
    u_xlat0.x = (u_xlatb0) ? 0.0 : 1.0;
    u_xlat8.x = (-_GlareFrontal) + _GlareBehind;
    u_xlat8.x = vs_TEXCOORD3.w * u_xlat8.x + _GlareFrontal;
    u_xlat8.x = (-u_xlat8.x) + 1.0;
    u_xlat8.x = u_xlat8.x * 1.5;
    u_xlat8.x = min(u_xlat8.x, _FresnelPow);
    u_xlat8.x = u_xlat8.x + (-_FresnelPow);
    u_xlat8.x = abs(u_xlat1.z) * u_xlat8.x + _FresnelPow;
    u_xlat12 = log2(abs(u_xlat1.z));
    u_xlat12 = u_xlat12 * 10.0;
    u_xlat12 = exp2(u_xlat12);
    u_xlat4 = u_xlat4 * u_xlat8.x;
    u_xlat4 = exp2(u_xlat4);
    u_xlat8.x = u_xlat4 * u_xlat0.x;
    u_xlat1.x = (-u_xlat0.x) * u_xlat4 + 1.00100005;
    u_xlat8.x = vs_TEXCOORD2.w * u_xlat1.x + u_xlat8.x;
    u_xlat0.x = u_xlat0.x * u_xlat4 + (-u_xlat8.x);
    u_xlat0.x = vs_TEXCOORD3.w * u_xlat0.x + u_xlat8.x;
    u_xlatb4 = u_xlat3.x>=1.0;
    u_xlat4 = u_xlatb4 ? 1.0 : float(0.0);
    u_xlat8.x = max(_ConeApexOffsetZ, 0.100000001);
    u_xlat1.x = u_xlat8.x + _DistanceFadeEnd;
    u_xlat8.x = u_xlat1.x / u_xlat8.x;
    u_xlat5 = (-u_xlat8.x) + 1.0;
    u_xlat8.x = u_xlat8.x / u_xlat1.x;
    u_xlat1.x = u_xlat5 / u_xlat1.x;
    u_xlat8.x = u_xlat1.x * u_xlat3.x + u_xlat8.x;
    u_xlat8.x = float(1.0) / u_xlat8.x;
    u_xlat8.x = u_xlat8.x + (-_ConeApexOffsetZ);
    u_xlat8.x = u_xlat8.x + -abs(vs_TEXCOORD0.z);
    u_xlat1.x = float(1.0) / _DynamicOcclusionDepthProps;
    u_xlat8.x = u_xlat8.x * u_xlat1.x;
    u_xlat8.x = clamp(u_xlat8.x, 0.0, 1.0);
    u_xlat1.x = u_xlat8.x * -2.0 + 3.0;
    u_xlat8.x = u_xlat8.x * u_xlat8.x;
    u_xlat5 = u_xlat8.x * u_xlat1.x;
    u_xlat8.x = (-u_xlat1.x) * u_xlat8.x + 1.0;
    u_xlat4 = u_xlat4 * u_xlat8.x + u_xlat5;
    u_xlat4 = u_xlat4 * vs_TEXCOORD5.w;
    u_xlat1 = texture(_VLB_NoiseTex3D, vs_TEXCOORD5.xyz);
    u_xlat8.x = u_xlat1.w + -1.0;
    u_xlat8.x = _NoiseParam.x * u_xlat8.x + 1.0;
    u_xlat1.x = (-u_xlat8.x) + 1.0;
    u_xlat8.x = u_xlat12 * u_xlat1.x + u_xlat8.x;
    u_xlat4 = u_xlat8.x * u_xlat4;
    u_xlat8.xy = vs_TEXCOORD6.xy / vs_TEXCOORD6.ww;
    u_xlat1 = texture(_CameraDepthTexture, u_xlat8.xy);
    u_xlat8.x = _ZBufferParams.z * u_xlat1.x + _ZBufferParams.w;
    u_xlat8.x = float(1.0) / u_xlat8.x;
    u_xlat8.x = u_xlat8.x + (-_ProjectionParams.y);
    u_xlat8.y = vs_TEXCOORD6.z + (-_ProjectionParams.y);
    u_xlat8.xy = max(u_xlat8.xy, vec2(0.0, 0.0));
    u_xlat8.x = (-u_xlat8.y) + u_xlat8.x;
    u_xlat12 = dot(unity_CameraWorldClipPlanes[4].xyz, unity_CameraWorldClipPlanes[5].xyz);
    u_xlatb12 = abs(u_xlat12)>=0.99000001;
    u_xlat12 = u_xlatb12 ? 1.0 : float(0.0);
    u_xlat12 = u_xlat12 * _DepthBlendDistance;
    u_xlat1.x = abs(vs_TEXCOORD0.z) / u_xlat12;
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
    u_xlat5 = u_xlat12 * u_xlat1.x;
    u_xlatb12 = 0.0>=u_xlat12;
    u_xlat12 = u_xlatb12 ? 1.0 : float(0.0);
    u_xlat1.x = u_xlat1.x * 100.0;
    u_xlat1.x = min(u_xlat1.x, 1.0);
    u_xlat8.x = u_xlat8.x / u_xlat5;
    u_xlat8.x = clamp(u_xlat8.x, 0.0, 1.0);
    u_xlat5 = (-u_xlat8.x) + 1.0;
    u_xlat8.x = u_xlat12 * u_xlat5 + u_xlat8.x;
    u_xlat8.x = u_xlat8.x + -1.0;
    u_xlat8.x = u_xlat1.x * u_xlat8.x + 1.0;
    u_xlat12 = (-u_xlat8.x) + 1.0;
    u_xlat8.x = unity_OrthoParams.w * u_xlat12 + u_xlat8.x;
    u_xlat4 = u_xlat8.x * u_xlat4;
    u_xlat8.x = abs(vs_TEXCOORD2.z) + (-_ProjectionParams.y);
    u_xlat8.x = u_xlat8.x / _DistanceCamClipping;
    u_xlat8.x = clamp(u_xlat8.x, 0.0, 1.0);
    u_xlat12 = u_xlat8.x * -2.0 + 3.0;
    u_xlat8.x = u_xlat8.x * u_xlat8.x;
    u_xlat8.x = u_xlat12 * u_xlat8.x + -1.0;
    u_xlat12 = max(vs_TEXCOORD2.w, unity_OrthoParams.w);
    u_xlat12 = (-u_xlat12) + 1.0;
    u_xlat8.x = u_xlat12 * u_xlat8.x + 1.0;
    u_xlat4 = u_xlat8.x * u_xlat4;
    u_xlat0.x = u_xlat0.x * u_xlat4;
    SV_Target0.w = u_xlat0.x * vs_TEXCOORD4.w;
    SV_Target0.xyz = vs_TEXCOORD4.xyz;
    return;
}

#endif
                                $Globals�         _ProjectionParams                            _ZBufferParams                          unity_OrthoParams                            unity_CameraWorldClipPlanes                  0      _ConeSlopeCosSin                  �      _ConeApexOffsetZ                  �      _DistanceFadeEnd                  �      _DistanceCamClipping                  �      _FresnelPow                   �      _GlareFrontal                     �      _GlareBehind                  �      _CameraParams                     �      _DynamicOcclusionDepthProps                   �      _DepthBlendDistance                   �      _NoiseParam                   �          $Globals�        _Time                            _WorldSpaceCameraPos                        _ProjectionParams                            unity_OrthoParams                     0      _AlphaInside                  �     _AlphaOutside                     �     _ConeRadius                   �     _AttenuationLerpLinearQuad                    �     _DistanceFadeStart                    �     _DistanceFadeEnd                  �     _FadeOutFactor                    �     _GlareFrontal                     �     _DrawCap                  �     _CameraParams                     �     _NoiseLocal                   �     _NoiseParam                   �     _VLB_NoiseGlobal                  �     unity_ObjectToWorld                  @      unity_WorldToObject                  �      unity_MatrixV                    �      unity_MatrixVP                         _ColorGradientMatrix                 @            _DynamicOcclusionDepthTexture                     _VLB_NoiseTex3D                 _CameraDepthTexture              