\contentsline {section}{\numberline {1}The Problem}{1}{section.1}%
\contentsline {subsection}{\numberline {1.1}The Magic Circle}{1}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Learning and Modeling Game AI}{2}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Hovercraft, Flying Fairies, Swimming Monsters, and Other Free-Range Driving Gameplay Mechanics}{3}{subsection.1.3}%
\contentsline {section}{\numberline {2}The Goal}{5}{section.2}%
\contentsline {section}{\numberline {3}Preliminaries of a Solution}{5}{section.3}%
\contentsline {subsection}{\numberline {3.1}Q-Learning}{6}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}The \textsl {Unity Engine}}{8}{subsection.3.2}%
\contentsline {section}{\numberline {4}Our Design}{10}{section.4}%
\contentsline {subsection}{\numberline {4.1}Use Cases}{11}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}\texttt {QLearning}}{12}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}\texttt {MovementQLearning}}{13}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}\texttt {TrackQLearning}}{14}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}Carts}{17}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}\texttt {Cart}}{17}{subsection.4.6}%
\contentsline {subsection}{\numberline {4.7}\texttt {CartState}}{19}{subsection.4.7}%
\contentsline {subsection}{\numberline {4.8}\texttt {AmbientStateSampler}}{20}{subsection.4.8}%
\contentsline {subsection}{\numberline {4.9}\texttt {QLearnedManipulator}}{22}{subsection.4.9}%
\contentsline {section}{\numberline {5}Results}{25}{section.5}%
\contentsline {subsection}{\numberline {5.1}The Game}{25}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}The Promise of Reinforcement Learning}{25}{subsection.5.2}%
