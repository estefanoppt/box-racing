\contentsline {section}{Contents}{}{section*.1}%
\contentsline {section}{\numberline {1}The Problem}{1}{section.1}%
\contentsline {subsection}{\numberline {1.1}The Magic Circle}{1}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Learning and Modeling Game AI}{2}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Hovercraft, Flying Fairies, Swimming Monsters, and Other Free-Range Driving Gameplay Mechanics}{3}{subsection.1.3}%
\contentsline {section}{\numberline {2}The Goal}{5}{section.2}%
\contentsline {section}{\numberline {3}Preliminaries of a Solution}{5}{section.3}%
\contentsline {subsection}{\numberline {3.1}Q-Learning}{6}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Why Q-Learning?}{6}{subsubsection.3.1.1}%
\contentsline {subsection}{\numberline {3.2}The Algorithm}{7}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}The Differences of our Q-Learning \emph {(optional)}}{9}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}The \textsl {Unity Engine}}{10}{subsection.3.4}%
\contentsline {section}{\numberline {4}Our Analysis}{13}{section.4}%
\contentsline {section}{\numberline {5}Our Meta-Design}{14}{section.5}%
\contentsline {subsection}{\numberline {5.1}Tracer Bullet Process}{15}{subsection.5.1}%
\contentsline {section}{\numberline {6}Our Design}{17}{section.6}%
\contentsline {subsection}{\numberline {6.1}\texttt {QLearning}}{18}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}\texttt {MovementQLearning}}{19}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}\texttt {Destination}}{21}{subsection.6.3}%
\contentsline {subsection}{\numberline {6.4}\texttt {TrackQLearning}}{21}{subsection.6.4}%
\contentsline {subsection}{\numberline {6.5}Carts}{24}{subsection.6.5}%
\contentsline {subsection}{\numberline {6.6}\texttt {Cart}}{25}{subsection.6.6}%
\contentsline {subsection}{\numberline {6.7}\texttt {CartState}}{26}{subsection.6.7}%
\contentsline {subsection}{\numberline {6.8}\texttt {AmbientStateSampler}}{27}{subsection.6.8}%
\contentsline {subsection}{\numberline {6.9}\texttt {QLearnedManipulator}}{29}{subsection.6.9}%
\contentsline {section}{\numberline {7}Results}{32}{section.7}%
\contentsline {subsection}{\numberline {7.1}Empirical Results}{32}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}The Game}{32}{subsection.7.2}%
\contentsline {subsubsection}{\numberline {7.2.1}A Game for Everyone}{33}{subsubsection.7.2.1}%
\contentsline {subsubsection}{\numberline {7.2.2}A Game for Game Designers}{34}{subsubsection.7.2.2}%
\contentsline {section}{\numberline {8}Conclusion}{35}{section.8}%
\contentsline {subsection}{\numberline {8.1}The Promise of Reinforcement Learning}{35}{subsection.8.1}%
\contentsline {section}{References}{37}{section*.2}%
