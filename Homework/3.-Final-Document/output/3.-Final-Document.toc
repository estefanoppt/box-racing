\contentsline {section}{Contents}{}{section*.1}%
\contentsline {section}{\numberline {1}The Problem}{1}{section.1}%
\contentsline {subsection}{\numberline {1.1}The Magic Circle}{1}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Learning and Modeling Game AI}{2}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Hovercraft, Flying Fairies, Swimming Monsters, and Other Free-Range Driving Gameplay Mechanics}{3}{subsection.1.3}%
\contentsline {section}{\numberline {2}The Goal}{5}{section.2}%
\contentsline {section}{\numberline {3}Preliminaries of a Solution}{5}{section.3}%
\contentsline {subsection}{\numberline {3.1}Q-Learning}{6}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Why Q-Learning?}{6}{subsubsection.3.1.1}%
\contentsline {subsection}{\numberline {3.2}The Algorithm}{7}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}The Differences of our Q-Learning \emph {(optional)}}{10}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}The \textsl {Unity Engine}}{11}{subsection.3.4}%
\contentsline {section}{\numberline {4}Our Analysis}{14}{section.4}%
\contentsline {subsection}{\numberline {4.1}Use Cases}{14}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Scenario: \texttt {View Mode::Start from Scratch}}{15}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}Scenario: \texttt {View Mode::Pre-Load Q-Values}}{16}{subsubsection.4.1.2}%
\contentsline {subsubsection}{\numberline {4.1.3}Scenario: \texttt {Race-Game} to Completion}{16}{subsubsection.4.1.3}%
\contentsline {subsection}{\numberline {4.2}Modeling the Agents}{18}{subsection.4.2}%
\contentsline {section}{\numberline {5}Our Meta-Design}{19}{section.5}%
\contentsline {subsection}{\numberline {5.1}Tracer Bullet Process}{20}{subsection.5.1}%
\contentsline {section}{\numberline {6}Our Design}{22}{section.6}%
\contentsline {subsection}{\numberline {6.1}\texttt {QLearning}}{23}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}\texttt {MovementQLearning}}{24}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}\texttt {Destination}}{26}{subsection.6.3}%
\contentsline {subsection}{\numberline {6.4}\texttt {TrackQLearning}}{26}{subsection.6.4}%
\contentsline {subsection}{\numberline {6.5}Carts}{29}{subsection.6.5}%
\contentsline {subsection}{\numberline {6.6}\texttt {Cart}}{29}{subsection.6.6}%
\contentsline {subsection}{\numberline {6.7}\texttt {CartState}}{30}{subsection.6.7}%
\contentsline {subsection}{\numberline {6.8}\texttt {AmbientStateSampler}}{32}{subsection.6.8}%
\contentsline {subsection}{\numberline {6.9}\texttt {QLearnedManipulator}}{34}{subsection.6.9}%
\contentsline {section}{\numberline {7}Implementation Models}{36}{section.7}%
\contentsline {section}{\numberline {8}Results}{37}{section.8}%
\contentsline {subsection}{\numberline {8.1}Empirical Results}{37}{subsection.8.1}%
\contentsline {subsection}{\numberline {8.2}The Game}{37}{subsection.8.2}%
\contentsline {subsubsection}{\numberline {8.2.1}A Game for Everyone}{37}{subsubsection.8.2.1}%
\contentsline {subsubsection}{\numberline {8.2.2}A Game for Game Designers}{39}{subsubsection.8.2.2}%
\contentsline {section}{\numberline {9}Impact}{40}{section.9}%
\contentsline {section}{\numberline {10}Conclusion}{40}{section.10}%
\contentsline {subsection}{\numberline {10.1}The Promise of Reinforcement Learning}{40}{subsection.10.1}%
\contentsline {section}{References}{42}{section*.2}%
