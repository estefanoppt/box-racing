\documentclass[a4paper,twocolumn,10pt]{article}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e}

\usepackage{graphicx}
\usepackage{eurosis}
\usepackage{url}
\usepackage{natbib}
\usepackage{amsfonts}
\usepackage{cases}
\usepackage{listings}
\lstset{language=C++}

% Bibliogray --------------------------------------------------------------------------------------
\bibliographystyle{eurosis}
\bibpunct[; ]{(}{)}{,}{a}{}{;}

% New commands ------------------------------------------------------------------------------------
\newcommand{\newterm}[1]{\textit{#1}}
\newcommand{\arrest}[1]{\textsl{#1}}
\newcommand{\variable}[1]{\textit{#1}}
\renewcommand{\function}[1]{\texttt{#1}}
\newcommand*\rfrac[2]{{}^{#1}\!/_{#2}}

\setlength{\parskip}{0pt} 
% Intro -------------------------------------------------------------------------------------------
\begin{document}

\title{TOWARDS TRAINING SWARMS FOR GAME AI }

\author{
	Estefano Palacios, Israel Pasaca, Vielka Villavicencio, and Enrique Pel\'aez \\
	Facultad de Ingenier\'ia El\'ectrica y Computaci\'on \\
	Escuela Superior Polit\'ecnica del Litoral \\
	Campus Gustavo Galindo \\
	Guayaquil, Ecuador \\
	E-mail: \texttt{\{espapala, epasaca, vvvillav, epelaez\}@espol.edu.ec}
}

\date{}

\maketitle

\thispagestyle{empty}

\keywords{Game design, game AI, reinforcement learning.}

\begin{abstract}
	This paper presents \newterm{The Swarm Intellligence and the
	Originality Kernel}, a game design pattern that can create
	a swarm of hundreds or thousands of hostile NPCs.
	It is driven by \newterm{insane-Q}: an onl[in]e, [sa]mpling-based, [n]on-[e]pisodic
	[Q]-Learning variant. To unify the swarm’s
	behaviors, individuals query a shared insane-Q instance. To
	unchain the individuals from behaving identically, the data
	they feed to insane-Q is entangled with a programmable handle,
	which enables individuals to, indirectly, manipulate how insane-Q
	interprets its shared learning. Following an experimental design
	methodology, a challenging racing mini-game was created through
	this pattern. It pits players against a reckless stampede of
	ninety-nine autonomous driving carts featuring two personalities: 
	\newterm{greedy carts} always take the path with the least amount
	of obstacles, while \newterm{lazy carts} always take the shortest path
	towards their next destination. 
\end{abstract}


\section{INTRODUCTION} % -----------------------------------------------------------------------------

\subsection{The Player vs. The Machine}
An essential practice in videogame design is to use \newterm{artificial
intelligence} (AI) to drive \newterm{non-player characters} (NPCs) whose
sole purpose is to improve the player's experience.[Yannakakis] If
an NPC is designed to be competitive, the clash between the player and the
NPC generates a dramatic experience about the relationship between power, strategy,
and wit.[Sheldon] The more the NPC portrays iteself, and behaves, as a sentient organism,
the more the player will enjoy being the victor of this challenge.[Eladhari]
 However, the space of possible experiences that can emerge from this
clash is constrained by the AI method chosen to drive the NPC's
behavior.[Yannakakis] Some methods yield NPCs whose entire bahavioral
space can be memorized by players, while other methods produce
NPCs so skillful most players cannot disentangle any of the NPC's strategies.

\subsection{AI Methods for NPC Design}
\newterm{Ad-hoc behavior authoring methods}---methods like finite state machines,
behavior trees, decision trees, fuzzy logic, and utility-based AI---have
dominated the practice of game AI since its early days.[Yannakakis] These methods
use static representations for the NPCs, and they dispend of searching and
learning in their execution. Thus, these methods are rarely used to craft NPCs that 
\newterm{play to win}: that is, NPCs that seek to play as optimally as possible in order to reach
a winning state of the game before other competing NPCs or other human
players.  Instead, since the behaviors that emerge from these NPCs are
predictable but, at least from a design perspective, highly malleable,
AI authors often seek to use these ad hoc methods to craft NPCs that
to some extend portray the \emph{illusion} of intelligence, while purposely
leaving discernible weakpoints in the NPCs behaviors that can be 
exploited by players.[Buckland] However, strategies to defeat these NPCs
can be learned by rut, and some players claim them to be boring.[Graepel]

\newterm{Reinforcement learning} (RL) stands in a different category
from ad-hoc methods. RL is the branch of machine learning where NPCs
learn appropriate behaviors by having them act on their environment and
internalize a measure of success that depends on the action’s effect on
that environment.[Barto] The NPC thus chooses the actions that have proven to
be most successful in the past. RL has been  used to craft NPCs that learn to play
existing games at super-human level performance, but it has been seldom used to
create NPCs that improve the quality of the player's experience.[Millington]

Given that RL is effective at producing intelligent behaviors, 
it would be fortuitous to discover a niche for RL in the
design of engaging NPCs that ad-hoc methods can not occupy.

\subsection{Experimental Design}
To discover novel applications of any AI technique to craft engaging
NPCs, the game and the technique of choice should evolve together,
in a fruitful cycle where the game feeds back to the technique, and
the technique feeds back to the game, seeking to harness any trait of the
AI technique that could fortify the game’s experience. This paper
refers to this process as \newterm{experimental design}.

In essence, this methodology is about letting the chosen AI technique
drive creative process: let it be the main problem statement guiding
Schell’s lenses[Schell], let it be the conceptual element of
Fullerton’s play-centric approach[Fullerton], let it be the central
challenge powering up Michalkow’s thinker toys[Michalkow], let it be
the primary constraint of Kultima’s game jam[Kultima]. Whichever the
methodology, engrave the AI technique at its core.

\subsection{A Game Design Pattern}
Inspired in the pattern-based methodology of architect Christopher
Alexander[Alexander], and inspired in the ominous beauty of insect
hordes, the present research applied experimental design, using RL
as the technological center of the creative process, to discover
a game design pattern named \newterm{The Swarm
Intelligence and the Originality Kernel}, which can engender 
swarms of hundreds or thousands of hostile NPCs that exhibit
highly coordinated behaviors, while 
imbuing swarm individuals with some degree of behavioral 
originality by granting them a means to manipulate,
in real time, the way the underlying RL algorithm interprets its learning.

\subsection{Paper Organization}
Section INSANE-Q explains the RL algorithm used to drive
this pattern. Section THE PATTERN explains where the idea for
the pattern originated, its participants, its interfaces, and its core algorithm. 
The section A RACING MINI-GAME details the major game design decisions that
emerged during the re-application of experimental design, using now this 
pattern as the technological center, to create 
a game about a reckless stampede of carts that race the player hecticly around a 
track. The section ADVICE proposes guidance on how to effectively 
apply this pattern based on the leasons learned during the creation 
of the racing mini-game. CONCLUSIONS and FUTURE WORK close this research.
 
\section{INSANE-Q} % ----------------------------------------------------------

\subsection{A Starting Point}
Q-Learning is an off-policy, temporal difference algorithm for
RL[Watkins]. To apply experimental design, Q-Learning
was chosen as this pattern’s creative driver for its shown 
convergence to optimality, its low conceptual barriers, 
and its ease of deployment. 

However, the original Q-Learning algorithm
[Millington][Sutton][Watkins]
has certain constraints which could stagnate designers’
imaginations, for research has found that the more constraints 
are imposed over creators, the more linear their creative process
becomes[Kahneman]. Thus, Q-Learning was groomed into \newterm{insane-Q}: 
a Q-Learning variant that sheds some of these restrictions.

\subsection{Q-Learning's Restrictions}
Traditional Q-Learning: 
i) is offline, for learning happens in a non-playable, non-real-time simulations of the game; 
ii) requires a fully-defined state-producing machinery for the NPC, a structure that can walk
through the NPC’s entire state space; 
iii) assumes that a terminal state for the NPC would be eventually found (a grotesque imposition,
as the NPCs’ states would need to explicitly capture the game’s
rules); 
iv) is episodic, and it automatically re-starts episodes once the NPC has reached a terminal state,
and v) bootstraps around the next simulated state of the NPC.

\subsection{Faster, Harder, Stronger}
insane-Q is shown on Algorithm 2. The next section outlines its behavior. For now, notice that it:
i) is online, for it does not drive a simulation, NPCs drive its execution; 
ii) uses sampled states, relinquishing the need for state-producing machinery; 
iii) does not require NPCs to define what a terminal state is;  
iv) is non-episodic; 
and v) bootstraps the \emph{previous} sampled state of the NPC onto its \emph{current} sampled state.

\begin{algorithm}\label{insane-Q}
	\small
	\SetAlgoLined
	\KwData{
	$Q(s, a)$ is a dictionary that saves the Q-values per-state, per-action; 
	$\rho \in [0, 1]$ is the exploration parameter;
	$\alpha \in [0, 1]$ is the learning rate;
	$\gamma \in [0, 1]$ is the discount rate.
	}
	
	\BlankLine
	
	\SetKwFunction{GetReward}{GetReward}
	\SetKwFunction{Random}{Random}
	\SetKwFunction{GetRandomAction}{GetRandomAction}
	\SetKwFunction{GetBestAction}{GetBestAction}
	\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}
	
	\Input{$s_{t - 1}$ is the previous sampled state, $a_{t - 1}$ is the previous action, $s_t$ is the current sampled state.}
	\Output{$a_t$ the next suggested action.}
	
	\BlankLine
	
	\BlankLine
	
	\SetKwProg{Fn}{Function}{ is}{end}
	\Fn{Learn($s_{t - 1}, a_{t - 1}, s_t)$: $a_t$}{
	\If{\Random{0, 1} $<$ $\rho$}{
	$a_t \leftarrow$ \GetRandomAction{$s_t$} 
	} \Else {
	$a_t \leftarrow$ \GetBestAction{$Q$, $s_t$} 
	}
	
	\BlankLine
	
	$r \leftarrow$ \GetReward{$s_t$} 
	
	\BlankLine

	$Q_{t - 1}$ \textbf{is} $Q(s_{t-1}, a_{t-1})$

	$Q_t$ \textbf{is} $Q(s_t, a_t)$

	\BlankLine

	$Q_{t-1} \leftarrow (1 - \alpha) \cdot Q_{t-1} + \alpha \cdot (r + \gamma \cdot Q_t)$

	
	\BlankLine
	
	\Return{$a_t$}
	}
	
	\BlankLine
	
	\caption{insane-Q}
\end{algorithm}

\subsection{Grooking insane-Q}
insane-Q works through a state-space $S$ that represents all possible
states of the NPC can reach. It must be discrete and countably finite. For
each state $s \in S$, there’s a set $A(s)$ which contains all actions
that the NPC can perform from the state $s$. insane-Q then requires
a look-up table $Q(s, a)$ of \newterm{quality-values}, where each
of these values is an estimated score of how much reward can the NPC procure,
looking ahead into the future, by taking action $a$ from state $s$. 
The goal of the algorithm is to execute until the $Q$ table stabilizes
such that it describes which actions should the NPC take to maximize
its reward during its involvement in the game. The algorithm is
initialized (usually[Millington]) so that $Q(s, a) = 0, \forall s \in S,
\forall a \in A(s)$.

Lines $2 \rightarrow 7$ select a \emph{greedy} action or an
\emph{exploratory} action, depending on the value of $\rho$. If, for
example, $\rho = \rfrac{1}{10}$, then \emph{one out of ten times}
insane-Q will select an action---not based on the best available
action---but at random. This is crucial so that the algorithm
continues exploration of new strategies and does not fall into a rut
of behavior. \function{GetRandomAction()} picks \emph{any} of the
actions $a \in A(s_t)$ available to the state $s_t$ with uniform
probability.  On the other hand---if again we assume that $\rho =
\rfrac{1}{10}$---\emph{nine out of ten times} insane-Q will select the
action $a$ for which $Q(s_t, a)$ is maximized. That is, we select the
action $a \in A(s_t)$ with the best Q-Value given our current state
$s_t$, with ties broken arbitrarily. This is the role of the function
\function{GetBestAction()}.

Line $8$ calls the function \function{GetReward()}, which must be
carefully designed to reward or punish the NPC for having arrived at
the particular state $s_t$. If the previous action $a_{t - 1}$ lead the
NPC to a state which we \emph{don't want to reinforce}, then we punish
the NPC by having \function{GetReward()} return a negative value.  If,
on the other hand, the state $s_t$ is one we \emph{wish to reinforce},
then \function{GetReward()} can reward the NPC by returning a positive value.

Line $9$ is where learning takes place. Notice that the value
$r + \gamma \cdot Q_{t}$ is propagated into $Q_{t-1}$. The idea is that if the
previous action $a_{t - 1}$ lead the NPC from state $s_{t - 1}$ to state $s_t$,
we want to leave a trace of how beneficial or how prejudicial that state
transition was.  In this manner, the next time the NPC is in the state $s_{t-1}$,
it has an even more ascertained approximation as to which action $A(s_{t-1})$
should be taken from that state. To learn, insane-Q assumes that the quality 
of an action $a_{t-1}$ leaving state $s_{t-1}$ can be solely judged based on
the reward $r$ and the currently-estimated quality-value $Q(s_t, a_t)$. Notice that if $r$ is a high positive value,
and $\gamma \cdot Q_t$ is a high negative value, this means we've arrived at
a highly rewarding state but from which no good actions can be taken. The
total propagated value is thus close to zero. Further, if $r$ is close to zero,
but $\gamma \cdot Q_t$ is well-above zero, this means that we reached a state
that doesn't directly derive any reward for the NPC, but from which other 
states that do seem promising in begetting reward can be reached. The parameter $\alpha$, also called the
\newterm{learning rate}, provides a trade-off on how incisive will this 
learning be. High values of $\alpha$ mean that insane-Q will quickly
forget what is has learned previously in favor for what it is learning
\emph{right now}. Low values of $\alpha$ allow the learning to be more
cautious, respecting what was learned before. Specifically, the backpropagation through $\alpha$ is designed
as an exponential average to improve the algorithm's performance in
non-stationary problems[Bartton].  The parameter $\gamma$, or discount
rate, controls how much of $Q_{t-1}$ percoletes onto $Q_t$. This $\gamma$ is
necesssary because we need a means to control how far can a quality-value 
propagate backwards to create a chain of optimal actions.

Finally, line $10$ returns the suggested action $a_t$.

\section{THE PATTERN} % ------------------------------------------------------------------------------

\subsection{Inspiration}
In applying experimental design, and by analyzing insane-Q’s essential properties, a naive intuition
emerged: if NPCs were to have a large number of states, and if each
NPC could execute large number of actions from each of those states,
then they could generate a vast amount of surprises for players, as
insane-Q would lead NPCs across a large spectrum of behaviors. This,
of course, implies large memory requirements as well, which means that,
at least for massive state spaces and action transitions, individual 
NPCs could not, each, own a different instance of insane-Q’s quality-value dictionary.

In insect sociaties, thought, agents act with a certain behavioral
unity [Holldobler]. So, for artificial swarms---where there could
be hundreds or even thousands of similarly-behaving NPCs---learning
could be shared, thus i) exploiting the need to reduce memory, ii)
permitting a large quality-value dictionary to participate in the game,
iii) accelerating learning, and iv) unifying the
NPCs’ behavior.

Now, while members in biological insect-societies share some of their
behaviors, sometimes there exist sub-classes or castes, each of which
displays some degree of originality. To unleash this originality in the
artificial swarms, NPCs could entangle the states they
feed into insane-Q with a programmable handle. Then, by having each
NPC sub-class maneuver that handle differently, NPCs could,
indirectly, manipulate how insane-Q interprets its training.
ersions of the original samples.

\subsection{Participants and Interfaces}

\subsubsection{State Sampler}
An object, shared by all individual NPCs of the swarm, that
provides the function \function{SampleRawState()}, which 
has the responsability of capturing the raw, continuous state of the
NPC at a specific time-frame. We call this unprocessed state the
\newterm{continuous-raw state} of the NPC, and it ought to have all the 
information insane-Q might need to learn to make intelligent 
decisions.

\subsubsection{Originality Kernel}
A fluctuating attribute that defines an NPC's \emph{mood}. All NPCs in the swarm 
share the same originality kernel \emph{concept}, but the mechanism
through which this kernel is calculated can vary between NPCs.
This is the programmable handle that is entangled to the 
continuous-raw states to provide a means for the NPCs to manipulate how
insane-Q interprets its learning.

\subsubsection{Originality Engine}
The element that manages the \emph{personality} which in turn
alters the \emph{mood} defined by the originality kernel. 
Different NPCs can have different originality engines. The
engine must provide the following interface:

\begin{itemize}
	\item \function{CaptureKernel()}: the heuristic function that
	calculates, for the current time-frame, the originality kernel for an NPC. 

	\item \function{KernelizeState()}: creates a \newterm{continuous-kernel state}
	whose elements are the corresponding elements of a continuous-raw state
	transformed in some manner by the originality kernel. The designer
	may choose to retain some elements unchanged.

	\item \function{DekernelizeAction()}: disentangles a \newterm{continuous-kernel action},
	produced by the swarm intelligence's insane-Q algorithm, from
	the originality kernel.
\end{itemize}

\subsubsection{Swarm Intelligence}
Represents the decision-making process shared by all the NPCs in the swarm: it is 
driven by a shared insane-Q instance. It must provide the following interface:

\begin{itemize}
	\item \function{QuantizeKernelState()}: quantizes a
	continuous-kernel state into a \newterm{quantized-kernel state}.

	\item \function{Learn()}: the insane-Q algorithm. Returns the
	best \newterm{quantized-kernel action}---according to what insane-Q has learned so far---that
	the NPC must take, given the current circumstances of the NPC.

	\item \function{DequantizeAction()}: dequantizes a quantized-kernel action
	insane-Q has produced.

	\item \function{GetReward()}: the specific reward function
	insane-Q will use to drive the swarm's training.


\end{itemize}

\subsubsection{Swarm Individual} 
An NPC belonging to the swarm. It must have the following
members:

\begin{itemize}
	\item \variable{self}: a reference to itself.

	\item \variable{SS}: a reference to the state sampler.
		
	\item \variable{OE}: a reference to a specific originality engine.

	\item \newterm{SI}: a reference to the centralized swarm intelligence. 

	\item \function{EnqueueAction()}: inserts an action into the
	NPC’s planner, scheduling it for execution.

	\item \function{FixedUpdate()}: an event that is triggered
	whenever the game engine has moved into a new physical game-play
	update for the game. The engine must promise to trigger this
	event for all NPCs in evenly-spaced periods of time, even if
	the rendering update falls behind.

	\item \variable{fixedDeltaTime}: the constant time that elapses
	between \function{FixedUpdate()} calls.
	
	\item \variable{timer}: regulates how often individuals query
	the swarm intelligence for their next decisions.

	\item \variable{decisionPeriod}: how often the NPC makes decisions.

	\item \variable{$s_{t-1}$}: the previous quantized-kernel state of the NPC.

	\item \variable{$a_{t-1}$}: the previous quantized-kernel action of the NPC.

\end{itemize}

\subsection{Core Algorithm}

Swarm individuals must transform their perceptions into actions. This
process is called \newterm{swarming}, and is summarized in Algorithm 1.
 
\begin{algorithm}\label{CastDecision}

	\small
	\SetAlgoLined

	\BlankLine

	\SetKwFunction{SampleRawState}{SampleRawState}
	\SetKwFunction{CaptureKernel}{CaptureKernel}
	\SetKwFunction{KernelizeState}{KernelizeState}
	\SetKwFunction{QuantizeState}{QuantizeState}
	\SetKwFunction{Learn}{Learn}
	\SetKwFunction{DequantizeAction}{DequantizeAction}
	\SetKwFunction{DekernelizeAction}{DekernelizeAction}
	\SetKwFunction{EnqueueAction}{EnqueueAction}

	\BlankLine

	\SetKwProg{Fn}{Function}{ is}{end}
	\Fn{Individual.FixedUpdate()}{


		$timer \leftarrow timer - fixedDeltaTime$

		\If{$timer > 0$}{ \Return{} }

		\BlankLine

		$s_t \leftarrow SS.$ \SampleRawState{$self$}

		$k \leftarrow OE.$ \CaptureKernel{$self$}

		$s_t \leftarrow OE.$ \KernelizeState{$s_t$, $k$}

		$s_t \leftarrow SI.$ \QuantizeState{$s_t$}

		\BlankLine

		$a_t \leftarrow SI. $ \Learn{$s_{t - 1}$, $a_{t - 1}$, $s_t$}

		\BlankLine

		$s_{t - 1} \leftarrow s_t$

		$a_{t - 1} \leftarrow a_t$

		\BlankLine

		$a_t \leftarrow SI.$ \DequantizeAction{$a_t$}
		
		$a_t \leftarrow OR.$ \DekernelizeAction{$a_t$}

		\EnqueueAction{$a_t$}

		\BlankLine

		$timer \leftarrow decisionPeriod$

		\Return{}
	}

	\BlankLine

\caption{Swarming}
\end{algorithm}

\subsection{Sub-Sampling}
Because we are dealing with quantized sampled states, allowing NPCs to
make decisions on every \function{FixedUpdate()} means that not enough time
may have elapsed in the simulation for there to be any noticeable difference
between the quantized states $s_{t-1}$ and $s_t$, \emph{even if} there's 
a difference between the corresponding continuous-raw states. That is, 
the action taken could ``lead'' to the same quantized state, 
and if that state produces a negative reward,
that action would be punished, even if it were to be the best action 
the NPC should take in that state. This fosters pathological learning. To
make insane-Q work in this context, the $decisionPeriod$ of the
swarm individual has to be set larger than $fixedDeltaTime$, so that
the algorithm perceives a difference between two subsequent states.

\subsection{Super-Sampling}
In some contexts, once training stabilizes and the NPCs are acting optimally, better
behavior can be coerced by stopping insane-Q’s learning process (by
setting $\alpha$ to $0$) and then forcing the NPCs to make more decisions
per second (by decreasing $decisionPeriod$). This works because insane-Q has
already stabalized: it has learned what are the best actions to take in all 
possible moments. We prune further learning so that pathological learning
does not taint the currently stored quality-values.

 

\section{A RACING MINI-GAME} % -----------------------------------------------------------------------

\subsection{Conceptualization}
The first step was to find a unifying vision for the game. Since a goal
was to show that RL techniques could create engaging
game-play, most of the game’s mechanics would have to be as simple
as possible, so that players would not find the fun in other aspects
of the game. That is, the fun should mainly to come from the behavior of the
AI. In a racing game, 
if one eliminates most dimensions from game-play
down to the genre's very essence, what remains for the player is the problem 
of \newterm{path-finding}.

To make the simple problem of path-finding engaging, the essential elements of racing games
were exaggerated: i) place a hundred carts on a small track, ii) make
them move at wildly different speeds, iii) train them to perform
well at dodging nearby obstacles, iv) make some of them very greedy at 
looping around the lap, and v) make them reckless in their
driving. With proper fine-tunning, these elements could lead carts to yield intricate, dynamic
paths. Furthermore, if the player is punished for hitting obstacles or
remaining static, then the issue of predicting how these living paths
will morph, and then skillfully maneuvering through them, could bind the
player to engagement. Finally, if the artificial carts and the player
pursue the maximization of similar rewards, then natural conflict would
emerge between them.  A vision-statement for the carts arrested these
colorful reveries in one thought: \arrest{create a stampede of carts that yields
a chaotic, dynamic maze in its wake.}

\subsection{Praxis}

\subsubsection{The Pattern's Essential Elements}
The carts’ originality kernel would be the so-called \newterm{chosen road direction} (CRD), 
which is a guiding beacon: a vector a cart is meant to
follow at all given moments to successfully loop around the track. How the CRD
is calculated is what gives different personalities to the different classes of carts. 
However, carts don't follow their CRD directly: they must first consult with the swarm intelligence.
For this swarm, the swarm intelligence's insane-Q algorithm would generate 
\newterm{steering actions} (SA), which are forces the carts need exert to 
i) swerve around threatening obstacles or, if the carts are free of danger, 
ii) align their trajectory to the CRD.

\subsubsection{Quantized-Kernel States}
The design of the carts’ quantized-kernel states sought to give
the swarm intelligence enough information so that it would learn as
many useful short-term strategies as possible. The definition of the
quantized-kernel states' evolved into the next structure:

\begin{enumerate}
	\item \newterm{ facingDirection: } the quantized direction the cart is facing,
	relative to its CRD.  0 means the cart is facing its CRD. $N/2$
	means the cart is facing in the direction opposite to its
	CRD. There are $N$ possible quantized directions. $N$ is a positive,
	even, configurable value of the system.

	\item \newterm{ degreeOfSpeed: } the quantized current speed of the car,
	relative to its maximum achievable speed. 0 means the cart is
	in standstill, $M$ means the cart is moving at its maximum
	speed. $M$ is a positive configurable value of the system.

	\item \newterm{isInRestartingState:} whether the cart has hit an object
	and is temporarily turned off.

	\item \newterm{ nearestObjectPosition: } the quantized position of the nearest
	object, relative to the cart’s CRD. 0 means the nearest object is
	located somewhere ahead in the direction of the cart’s CRD. $N/2$
	means the nearest object is located somewhere along
	the direction contrary to the cart’s CRD. A value of -1 means
	there’s no object nearby.

	\item \newterm{ nearestObjectNormalizedVelocity: } the quantized, normalized
	velocity of the nearest object, relative to the cart’s CRD. 0
	means the nearest object is moving in the direction of the cart’s
	CRD. $N/2$ means the object is moving contrary to the cart’s
	CRD. A value of -1 means there’s no object nearby.

	\item \newterm{ nearestObjectDegreeOfSpeed: } A value, between 0 and
	$M$, that represents the quantized speed of the
	nearest object, relative to its maximum achievable speed. A value
	of -1 means there’s no object nearby. 
\end{enumerate}

Note that the CRD is not fed directly to the swarm intelligence:
it is not part of the cart’s quantized-kernel states. It’s
information is, however, indirectly encoded into these states through
the kernelization process. To kernelize any of the
above directions, we use Algorithm 3, which makes a planar direction
$d$ \emph{relative} to the cart's $CRD$.

\begin{algorithm}\label{KernelizeDirection}

	\small
	\SetAlgoLined

	\SetKwFunction{Atan}{Atan2}
	\SetKwFunction{FromEuler}{FromEuler}
	\SetKwFunction{RotationFromQuaterion}{RotationFromQuaterion}
	\SetKwFunction{Normalize}{Normalize}
	\SetKwFunction{QuantizeState}{QuantizeState}
	\SetKwFunction{MultiplyVector}{MultiplyVector}
	\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}

	\Input{the cart's $CRD \in \mathbb{R}^3$ and $d \in \mathbb{R}^3$, where $d_y = 0$.}
	\Output{the direction $d$ kernelized to the $CRD$.}

	\BlankLine

	\SetKwProg{Fn}{Function}{ is}{end}
	\Fn{KernelizePlanarDirection}{

		$\theta \leftarrow Math.$\Atan{$CRD$.x, $CRD$.z}

		$Q \leftarrow Quaternion$.\FromEuler{0, -$\theta$, 0}

		$M \leftarrow Matrix3x3$.\RotationFromQuaterion{$Q$}

		$n \leftarrow Vector3.$\Normalize{$d$}

		$k \leftarrow M$.\MultiplyVector{$n$}

		\Return{$k$}
	}
	\caption{Kernelization of Carts' Directions}
\end{algorithm}

To quantize the continuous-kernel states' directions, Algorithm 4 was
used.

\begin{algorithm}\label{QuantizeKernelDirection}

	\small
	\SetAlgoLined

	\SetKwFunction{Atan}{Atan2}
	\SetKwFunction{Ceiling}{Ceiling}
	\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}

	\Input{a kernelized direction $k \in \mathbb{R}^3$, where $k_y = 0$, and $N$, the quantization resolution.}
	\Output{$k$ quantized into a integer belonging to $[0, N - 1]$.}

	\BlankLine

	\SetKwProg{Fn}{Function}{ is}{end}
	\Fn{QuantizeKernelVector}{
		$\theta \leftarrow Math.$\Atan{$k$.x, $k$.z}

		\If{$\theta < 0$}{ $\theta \leftarrow \theta + 2\pi$ }
		
		\If{$\theta > 2\pi - \rfrac{\pi}{N}$ \texttt{or} $\theta < \rfrac{\pi}{N}$}{
			\Return{$0$}
		}

		\Return{$Math$.\Ceiling{$\frac{\rfrac{N}{\pi}\cdot \theta - 1}{2}$}}
	}
	\caption{Quantization of Carts' Kernelized Directions}
\end{algorithm}

\subsubsection{Action Transitions}
There are $N$ discretized
SAs leading out of every quantized-kernel state. These SAs are also
relative to the CRD. An SA with a value of 0 means the swarm intelligence
is suggesting the cart move towards its CRD, a value of $N/2$ means
the swarm intelligence is suggesting the cart move contrary to its CRD.

\subsubsection{Memory Requirements}
With the chosen resolution for the different quantized values,
the insane-Q quality-value dictionary for this game has 295,936
distinct states and 4,734,976 action transitions, filling over 400MB of
main memory.

\subsubsection{Laps}
To simplify the calculation of the CRD, and to give players a goal to
strive for, a colluding element was added to the race tracks: \newterm{laps}. Laps
are triggers placed in such a way that: i) the next lap must be completely
visible from the prior lap, ii) a lap’s expanse must touch two walls,
iii) the space between two laps is canalized by walls, and iv) the
collection of all laps must complete the circuit, around the track,
we want our NPCs to travel.

\subsubsection{The Reward Function}
The behavioral goal for the swarm intelligence’s reward function
was to coerce carts to move aligned to their CRD, while letting them
learn strategies that would let them swerve around moving obstacles if
necessary. It is detailed in Algorithm 5

\begin{algorithm}\label{CartsGetReward}

	\small
	\SetAlgoLined

	\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}

	\Input{a state $s \in S$}
	\Output{the reward signal for state $s$}

	\BlankLine

	\SetKwProg{Fn}{Function}{ is}{end}
	\Fn{GetReward}{

		\If{$s.isInRestartingState$}{ \Return{ -1 } }

		$speedSignal \leftarrow s.degreeOfSpeed / M$

		$facingSignal \leftarrow f(s.facingDirection / N)$

		\Return{$speedSignal \cdot facingSignal$}
	}
	\caption{Cart's \texttt{GetReward} Function}
\end{algorithm}

Where the function $f(x)$ is defined as:

\begin{equation}
        f(x)=
        \left\{ 
		\begin{array}{ll}
			  \rfrac{-9}{2}x + 1 		&  0 \le x < \rfrac{1}{4} \\ \\
			  \rfrac{-1}{8}      		&  \rfrac{1}{4} \le x \le \rfrac{3}{4} \\ \\
			  \rfrac{9}{2}x - \rfrac{7}{2}  &  \rfrac{3}{4} < x \le 1 
		\end{array} 
	\right.
\end{equation}

Notice that with this design, Carts are: i) more rewarded the more they
are driving aligned to the CRD; ii) more punished the more they are
moving contrary to the CRD; iii) more rewarded, or more punished, the
faster the cart is going, according to whether its aligned to the its
CRD or not; iv) severely punished after colliding with an obstacle;
and v) continue to be punished as long as they are in restarting state.

\subsubsection{The Rules of the Game}
The rules of the game evolved in tandem with the cart’s reward
function. They are applied equally to both the player cart and the
artificial carts: i) a cart has real-valued \newterm{points}, which start
at 0; ii) a cart looses a trickle of points in every frame; iii) a cart wins 3
points for every lap it crosses; iv) a cart looses 2 points for every collision;
v) if a cart collides with an obstacle, its ability to move is shut down
for a small amount of time; vi) shut-down carts are not further punished by
collisions; vii)  shut-down carts are affected by rule 2; viii) a timer is set to
3 minutes; ix) by the end of the timer, the cart with the highest amount of
points in the track wins the game.

\subsubsection{The Player's Interface}
The player plays with a joystick. The cart accelerates in the direction
of the left stick. The player can also dampen the cart's acceleration with the right
trigger (a power artificial carts don't have).

\subsubsection{Greedy Carts}
An originality engine was designed, called the \newterm{greedy cart}. On
every tick of the decision timer, a greedy cart launches—starting from
its current position—spheres targeting evenly distributed positions
towards the next lap. The normalized direction of the sphere that hits
the least amount of obstacles is considered the greedy cart’s \newterm{desired
intention}. The closer the greedy cart is to the prior lap, the more its
actual CRD equals its desired intention. The closer the greedy cart is
to the next lap, the more its actual CRD points towards the point, on the
next lap’s surface, that’s closest to the greedy cart.  This creates a behavior
where greedy carts are always pursuing the clearest path towards the next
lap, without “going all the way around” once there are close to that
lap. 

\subsection{Training}

At first, training was carried without sub-sampling, which means that
$decisionPeriod = fixedDeltaTime$. The result was that the carts did 
not converge to any kind of intelligent decision-making. 
Now, when the carts were trained with a $decisionPeriod$ of 0.2 [sec], which made
them have a similar response-time to human drivers, training converged
to intelligent decision-making as they, as a collective, attained a
performance bounded by little over 6 [collisions per second].


Then, in applying super-sampling by changing the individual’s $decisionPeriod$ from
0.2 [sec] to 0.02 [sec], the performance of the algorithm improved the
collisions per second of the collective by 1200\%, dropping the metric
from 6 [collisions per second] to less than 0.5 [collisions per second].
Note that for this improvement to take effect the engine had to be configured to perform 
60 [physical updates per second], twice of its default, 30.

To gauge the performance of insane-Q in improving the carts' short-term
decision-making process, the carts were made to accelerate in the direction of their
CRD directly, without the aid of insane-Q to handle obstacles. This
resulted in a collective performance of 17.5 [collisions per second].

These results are summarized in Figure I.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.5]{./art/metrics.png}
	\caption{Performance of different insane-Q applications.}
\end{figure}


\subsection{Iteration}

\subsubsection{A Dominant Strategy}
Eight players played the game for as many rounds as they desired. 
A dominant strategy[Schell] was found:
since, by definition, tracks were all closed circuits, players could
win with relative ease by driving close to the inner walls of the tracks.

\subsubsection{Lazy Carts}
A new originality engine was crafted to nullify this dominant
strategy. This sub-class was called the \newterm{lazy cart}. A lazy cart’s CRD
always points towards the position, on the next lap’s surface, that’s
closest to it. This made lazy carts drive, mostly, near the inner walls
of the track, making passage through the path of the dominant strategy
difficult for players. To derive more fun out of this improvement, the
endpoint of the lazy cart’s CRD was artificially shifted, outwards, by a
random value between 0 and 2 meters. By doing this, lazy carts sometimes
leave an opening in the path through the dominant strategy. Since its
dangerous to go through that path, but at the same time very rewarding,
this behavior turned into an element of triangularity[Schell] as players
must take a bet as to whether they would successfully navigate through
that narrow ravine. Note that both greedy carts and lazy carts share the
same swarm intelligence, and thus the same underlying quality-values. The
only trait that varies between them is their originality engine: they
calculate their CRDs differently.

\subsubsection{Player Satisfaction}
Another round of informal testing was carried, this time combining
subjective quality metrics and interviews. 29 players played the game
and were asked to rate the game’s experience with a number between
1 and 7. They were also asked to describe their subjective thoughts on
any element of the game. The average rating was 6.2, and their written
narrative revealed that most players were marveled by the game’s
AI. 

\section{ADVICE}  % ----------------------------------------------------------------------------------
On applying this pattern, you must:
i) envision the concept, for the originality kernel, that will define the root cause of the NPCs personality fluctuation;
ii) design the state space for the NPCs by thinking on the information they’ll need to learn to behave optimally;
iii) find a kernelization process that entangles these states to the originality kernel in such a way that when the kernel fluctuates, the set of states the agent perceives are exactly those that you need;
iv) model a method for quantization with the appropriate resolution, one that allows for smooth NPC behavior;
v) select action transitions according to the behaviors you want NPCs to exhibit during game-play;
vi) design a reward function that will, through training, steer NPCs to desired behavior;
vii) design any originality engines needed to fill gaps in the game’s design;
viii) be cautious when you fail to propagate information about siblings into the continuous-raw states, for this disconnects the swarm;
ix) mind that, even if shared, high-resolution quantized-kernel state spaces eagerly devour main memory;
x) be wary of destructive kernelization processes, do avoid unintended annulment of any of the state space’s regions;
xi) verify that, between all NPC sub-classes, the entire state space is observed during game-play;
xii) look for signs  where the NPCs are behaving erradicatly, and then make sure training is saturating the swarm’s entire state space;
xiii) iterate, for sometimes the behaviors you seek can’t emerge from the state space, action transitions, and reward function you have designed;

\section{CONCLUSION} % -------------------------------------------------------------------------------
The Hive-Mind and the Originality Kernel is a generalized game design
pattern that can inspire the creation of autonomous swarms for game
AI. By relying on a clear metaphor, kernelization, and the lean insane-Q
algorithm, game designers are galvanized to weave engaging patterns
of behavior.

\section{FUTURE RESEARCH} % --------------------------------------------------------------------------

Instead of using insane-Q, deep reinforcement learning could increase
the applicability of the technique, because in its current form, this
technique forces the designer to craft a quantization process and choose
an appropriate sampling rate, which in many settings can be challenging.


\bibliography{refs}

\end{document}

